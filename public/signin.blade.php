@extends('layout.auth')

@section('content')


<!-- Page Content -->
<div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-4">
        
           
      
            <form class="well" action="dash-staff.php">
                            <div class="heading text-center">
                                <h4 class="text-brandon text-uppercase space-lg text-center text-white"><img src="{{assets('/public/assets')}}img/logomark.png" alt="" height="35px"></h4>
                                <span class="text-uppercase vla-red-text space-lg small">academy</span>
                            </div>
                            
                            <div class="alert alert-danger alert-auth small">
                                <button class="close" data-dismiss="alert" type="button">×</button> 
                                <strong>Gbaun!</strong>
                                Error message comes here. Your credentials do not match the list on the Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                            </div>
        
                            <div class="col-md-12">
                              
      
                                <label class="small text-uppercase control-label" for=
                                "inputUser">Username</label>
        
                                <div class="controls">
                                    <input class="input-lg form-control" id="inputUser" placeholder=
                                    "Enter Staff ID here" type="text">
                                </div>
                                <br>
                            </div>
        
                            <div class="col-md-12">
                                <label class="small text-uppercase control-label" for=
                                "inputPassword">Pass Code</label>
        
                                <div class="controls">
                                    <input class="input-lg form-control" id="inputPassword" placeholder=
                                    "Enter pass code" type="password">
                                </div>
                            </div>
        
                            <div class="col-md-12">
                                <div class="controls text-center"><br>
                                    <button class="btn btn-success btn-block btn-lg text-uppercase small" type="submit">Sign In &nbsp; <i class="small fa fa-arrow-right"></i></button> 
                                    <!-- <button class="btn" type= "button">Help</button> -->
                                </div><br>
                                <p class="text-muted small"> If you cannot remember your Sign in credentials, check the mail that was sent to you by the admin. Having Problems signin in still? Kindly contact administrator.</p>
                            </div>
        
            </form>
      
            <div class="">
            <small class="center-block text-center text-white">&copy; 2016. <a href="_index.php" class="text-white">Vic Lawerence & Associates</a></small>
              <br>
            </div>
        </div>
    </div>
</div>

@stop
<!-- /.container --> 