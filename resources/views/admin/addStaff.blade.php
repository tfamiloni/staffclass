@extends('layout.secondary-admin') @section('content')

<section id="addStaff" class="manager-dash bg-grey">
	<div class="container-fluid">
		<div class="row">

			<div class="col-sm-8 col-sm-offset-2">
				<div class="row">
				<div class="btn-group pull-right">
					<a href="" type="button" class="btn btn-default"><i class="fa fa-user-plus"></i>&nbsp; Add New Staff</a>
					<a href="" type="button" class="btn btn-default"><i class="fa fa-file-excel-o"></i>&nbsp; Bulk Upload Staff</a>
				</div>
				<h2 class="text-300 pull-left">Add Staff</h2>
				</div>
				<div class="row">
					<div class="col-sm-4 no-padding">
						<div class="well staff-tab text-center no-border profile-bg">
							<img src="//placehold.it/150x150" alt="profile image" width="150px" height="150px" class="staff-avatar">
							<a class="btn btn-default" href=""><i class="fa fa-photo">&nbsp;</i> Change Image</a>
						</div>
					</div>
					<div class="col-sm-8 no-padding">
						<div class="well staff-tab">
							<h5>First Name</h5>
							<input type="text" class="mng-input col-xs-12" placeholder="Enter First Name Here...">
<!--							<br><br>-->
							<h5>Last Name</h5>
							<input type="text" class="mng-input col-xs-12" placeholder="Enter Last Name Here...">
<!--							<br><br>-->
							<h5>Position</h5>
							<input type="text" class="mng-input col-xs-12" placeholder="Eg. Business Associate">
							<div class="separator-sm"></div>
							<div id="submit">
								<a class="btn btn-success" type="submit" data-toggle="modal" data-target="#staffSuccess">Generate password &amp; send mail</a>
							</div>
						</div>
					</div>
				</div>
				
<!--				Modal-->
				<div class="modal fade" id="staffSuccess" tabindex="-1" role="dialog" aria-labelled-by="addStaffSuccess">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header no-border">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							</div>
							<div class="modal-body text-center">
								<i class="success-icon glyphicon glyphicon-ok-circle"></i>
								<h3 class="text-brandon">Staff Added Successfully</h3>
							</div>
							<div class="modal-footer no-border">
								<a href="addStaff" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-user-plus"></i>Add New Staff</a>
								<a href="" type="button" class="btn btn-default" data-dismiss="modal">Bulk Upload Staff</a>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>
@include ('includes.footer') @stop @section('script') @endsection