@extends('layout.secondary-admin') @section('content')
<header class="vla-bg-header vla-header">
	<div class="container-fluid">
		<h2 class="text-white col-sm-6 no-margin no-padding">Manage Staff</h2>
		<div class="btn-quick-actions no-margin">
			<div class="col-sm-3">
				<a href="addStaff" type="button" class="btn btn-default btn-block"><i class="fa fa-user-plus"></i>&nbsp; Add New Staff</a>
			</div>
			<div class="col-sm-3">
				<a href="" type="button" class="btn btn-default btn-block"><i class="fa fa-file-excel-o"></i>&nbsp; Bulk Upload Staff</a>
			</div>
		</div>
	</div>
</header>
<section class="manager-dash manage-course bg-grey">
	<div class="container-fluid">
		<div class="well">
			<div class="row">
				<div class="col-sm-9 animated slideInLeft">
					<table class="table table-responsive col-sm-12 staff-table">
						<thead>
							<tr>
								<td align="right">
									<select class="form-control" style="width:200px; display:inline">
										<option value="">Bulk Actions</option>
										<option value="">Delete</option>
									</select>
									<button class="btn btn-default">Apply</button>
								</td>
								<td class="mng-checkbox">
									<input type="checkbox" id="checkbox-1" class="regular-checkbox">
									<label for="checkbox-1"></label>
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div class="row">
										<div class="col-sm-2">
											<img src="https://media-akam.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAYAAAAAJGUwZTQyZDg1LWM5MmQtNDdhYy04MTMyLWVjNjhmNDk5ZjY1ZA.jpg" alt="" height="75px" width="75px">
										</div>
										<div class="col-sm-10">
                                            <h4><a href="staff">Bidemi Oyebode</a> &#8226; <small>Completed 8 courses &#8226; 3 ongoing courses</small></h4>
											<p><span class="">Engineer</span> at <span>British American Tobacco</span></p>
											<div class="">
												<a href="" class="btn btn-default"> <i class="fa fa-edit"></i>&nbsp;Edit</a>
												<a href="" class="btn btn-default" data-toggle="modal" data-target="#delete-staff"><i class="fa fa-trash"></i>&nbsp;Delete</a>
											</div>
										</div>
									</div>
								</td>
								<td class="mng-checkbox">
									<input type="checkbox" id="checkbox-1-1" class="regular-checkbox">
									<label for="checkbox-1-1"></label>
								</td>
							</tr>

							<tr>
								<td>
									<div class="row">
										<div class="col-sm-2">
											<img src="https://media-akam.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAIKAAAAJDhmNzJkYmVlLTZiZmQtNDZkMC1iMDMyLTExZjFhZjQwOTMyNA.jpg" alt="" height="75px" width="75px">
										</div>
										<div class="col-sm-10">
                                            <h4><a href="staff">Henry Igugu</a> &#8226; <small>Completed 8 courses &#8226; 3 ongoing courses</small></h4>
											<p><span class="">Engineer</span> at <span>British American Tobacco</span></p>
											<div class="">
												<a href="" class="btn btn-default"> <i class="fa fa-edit"></i>&nbsp;Edit</a>
												<a href="" class="btn btn-default" data-toggle="modal" data-target="#delete-staff"><i class="fa fa-trash"></i>&nbsp;Delete</a>
											</div>
										</div>
									</div>
								</td>
								<td class="mng-checkbox">
									<input type="checkbox" id="checkbox-1-2" class="regular-checkbox">
									<label for="checkbox-1-2"></label>
								</td>
							</tr>

							<tr>
								<td>
									<div class="row">
										<div class="col-sm-2">
											<img src="https://media-akam.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAASxAAAAJGI1YTk3OGNiLWFkZDgtNGI5My04NDM3LTFlZjZkZGJkMDEwMA.jpg" alt="" height="75px" width="75px">
										</div>
										<div class="col-sm-10">
                                            <h4><a href="staff">Bidemi Oyebode</a> &#8226; <small>Completed 8 courses &#8226; 3 ongoing courses</small></h4>
											<p><span class="">Engineer</span> at <span>British American Tobacco</span></p>
											<div class="">
												<a href="" class="btn btn-default"> <i class="fa fa-edit"></i>&nbsp;Edit</a>
												<a href="" class="btn btn-default" data-toggle="modal" data-target="#delete-staff"><i class="fa fa-trash"></i>&nbsp;Delete</a>
											</div>
										</div>
									</div>
								</td>
								<td class="mng-checkbox">
									<input type="checkbox" id="checkbox-1-3" class="regular-checkbox">
									<label for="checkbox-1-3"></label>
								</td>
							</tr>

							<tr>
								<td>
									<div class="row">
										<div class="col-sm-2">
											<img src="https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAOcAAAAJGNkZGFjMTU2LTJmNmQtNDAwNS05ZjMyLTEyNTA5NDhjZTFjNg.jpg" alt="" height="75px" width="75px">
										</div>
										<div class="col-sm-10">
                                            <h4><a href="staff">Henry Igugu</a> &#8226; <small>Completed 8 courses &#8226; 3 ongoing courses</small></h4>
											<p><span class="">Engineer</span> at <span>British American Tobacco</span></p>
											<div class="">
												<a href="" class="btn btn-default"> <i class="fa fa-edit"></i>&nbsp;Edit</a>
												<a href="" class="btn btn-default" data-toggle="modal" data-target="#delete-staff"><i class="fa fa-trash"></i>&nbsp;Delete</a>
											</div>
										</div>
									</div>
								</td>
								<td class="mng-checkbox">
									<input type="checkbox" id="checkbox-1-4" class="regular-checkbox">
									<label for="checkbox-1-4"></label>
								</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-responsive col-sm-12">
						<tr>
							<td>
								<div class="form-inline">
									<ul class="pagination">
										<li><a href="#" aria-label="First">First</a>
											<li><a href="#" aria-label="Previous">Previous</a>
											</li>
											<li class="active"><a href="#">1</a></li>
											<li><a href="#" aria-label="Next">Next</a>
												<li><a href="#" aria-label="Last">Last</a>
									</ul>
								</div>
							</td>
							</tr>
					</table>
				</div>
				<div class="col-sm-3 animated slideInRight">
					<div class="well vla-bg-black">
						<div class="filter-section">
							<label for=""><i class="fa fa-search"></i>&nbsp; Search</label>
							<div class="input-group">
								<input type="text" class="form-control no-bod-rad" placeholder="Enter keyword">
								<span class="input-group-btn">
									<button class="btn btn.default"><i class="fa fa-search"></i></button>
								</span>
							</div>
						</div>
						<div class="filter-section">
							<label for=""><i class="fa fa-venus-mars"></i>&nbsp; Gender</label>
							<select name="" id="" class="form-control">
								<option value="">Select Gender</option>
								<option value="">Male</option>
								<option value="">Female</option>
							</select>
						</div>
						<div class="filter-section">
							<label for=""><i class="fa fa-building-o"></i>&nbsp; Company</label>
							<select name="" id="" class="form-control">
								<option value="">Select Company</option>
								<option value="">GT Bank</option>
								<option value="">Dangote Group</option>
								<option value="">British American Tobacco</option>
								<option value="">Microsoft Nigeria</option>
								<option value="">Konga</option>
								<option value="">UAC Foods</option>
							</select>
						</div>
						<div class="filter-section">
							<label for=""><i class="fa fa-industry"></i>&nbsp; Department</label>
							<select name="" id="" class="form-control">
								<option value="">Select Department</option>
								<option value="">Customer Service</option>
								<option value="">Computer and IT</option>
								<option value="">Engineering</option>
								<option value="">Human Resource</option>
								<option value="">Supply Chain</option>
								<option value="">Warehouse Management</option>
							</select>
						</div>
						<div class="filter-section">
							<label for=""><i class="fa fa-map-marker"></i>&nbsp; Location</label>
							<select name="" id="" class="form-control">
								<option value="">Select Location</option>
								<option value="">Abuja</option>
								<option value="">Lagos</option>
								<option value="">Ibadan</option>
								<option value="">Port Harcourt</option>
								<option value="">Kano</option>
								<option value="">Benin</option>
							</select>
						</div>
						<br>
						<div class="filter-section">
							<button class="btn btn-block btn-success btn-lg">Search</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--Modal-->
<div class="modal fade" id="delete-staff" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" arial-labelledby="confirm-delete">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="confirm-delete">Delete this Staff?</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete this Staff's record permantely</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
			</div>
		</div>
	</div>
</div>

@include ('includes.footer') @stop @section('script') @endsection