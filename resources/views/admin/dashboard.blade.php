@extends('layout.secondary-admin') @section('content')
<!-- Page Content -->
<header class="course-header dash no-bg-img">
	<div class="container-fluid">
		<div class="row">
			<div class="manager-head">
			<div class="animated bounceIn">
				<div class="col-md-1 col-sm-2 col-xs-3">
					<img src="{{asset('assets/img/user-male.png')}}" alt="" class="img-circle" width="60px" height="60px">
				</div>
				<div class="col-md-5 col-sm-10 col-xs-9">
					<p class="lead"><span class="text-white text-600">Welcome Admin Ayotola,</span>
						<br> What would you like to do today &rarr;</p>
				</div>
				</div>
				<div class="col-md-2 col-xs-12 animated bounceIn">
					<a href="addStaff" class="btn btn-default btn-block btn-quick-actions" style="color:rgb(237,125,23);"><i class="fa fa-users hidden-sm"></i>&nbsp; Add Staff</a>
				</div>
				<div class="col-md-2 col-xs-12 animated bounceIn">
					<a href="addManager" class="btn btn-default btn-block btn-quick-actions" style="color:rgb(224,39,29);"><i class="fa fa-users hidden-sm"></i>&nbsp; Add Manager</a>
				</div>
				<div class="col-md-2 col-xs-12 animated bounceIn">
					<a href="addCourse" class="btn btn-default btn-block btn-quick-actions"><i class="fa fa-book hidden-sm"></i>&nbsp; Add Course</a>
				</div>
			</div>
		</div>
	</div>
</header>

<section class="manager-dashboard admin-dash">
	<div class="container-fluid">
		<div class="row manager-section">
			<div class="panel-group row">
				<div class="col-sm-7">
					<div class="panel panel-default animated fadeInLeft" id="staff">
						<div class="col-md-5 col-sm-6 manager-tab-left">
							<div class="text-white text-center">
								<h2 class="text-white text-brandon no-margin">Staff</h2>
							</div>
						</div>
						<div class="col-md-7 col-sm-6 manager-tab-right">
							<h4 class="vla-orange-text">250 Staffs</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio adipisci assumenda quisquam fugiat, dicta perferendis.</p>
							<a href="manageStaff" class="btn btn-staff btn-primary btn-lg">Manage Staff</a>
						</div>
					</div>

					<div class="panel panel-default animated fadeInLeft" id="manager">
						<div class="col-md-5 col-sm-6 manager-tab-left">
							<div class="text-white text-center">
								<h2 class="text-white text-brandon no-margin">Managers</h2>
							</div>
						</div>
						<div class="col-md-7 col-sm-6 manager-tab-right">
							<h4 class="vla-red-text">16 Managers</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti corrupti, ut neque sunt. Voluptatum enim placeat iure necessitatibus, magni odio.</p>
							<a href="manageManager" class="btn btn-manager btn-primary btn-lg">Manage Managers</a>
						</div>
					</div>

					<div class="panel panel-default animated fadeInLeft" id="course">
						<div class="col-sm-6 col-md-5 manager-tab-left">
							<div class="text-white text-center">
								<h2 class="text-white text-brandon no-margin">Courses</h2>
							</div>
						</div>
						<div class="col-sm-6 col-md-7 manager-tab-right">
							<h4>75 Courses &#9679; 22 Drafts</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium nihil delectus autem totam accusantium omnis quos natus adipisci excepturi nemo quidem magni sunt aspernatur, aperiam, fugiat, eligendi minus ipsam doloribus.</p>
							<a href="manageCourse" class="btn btn-course btn-lg ">Manage Courses</a>
						</div>
					</div>
				</div>


				<div class="col-sm-5">
					<div class="panel panel-default panel-notify animated fadeInRight">
						<div class="discussion-header bg-grey">
							<h4 class="no-margin text-center vla-orange-text">
								Notifications
								<span class="label vla-red-bg text-white">3 new</span>
							</h4>
						</div>
						<div id="comment" class="comment">
							<div class="media">
								<br>
								<div class="media-left">
									<a href="#">
										<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
									</a>
								</div>

								<div class="media-body">
									<h4 class="media-heading">Bolatito Ogunmodede <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria</a>
                                </h4>

									<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra...</p>

									<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>


								</div>
								<hr class="no-margin">
							</div>

							<div class="media">
								<div class="media-left">
									<a href="#">
										<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
									</a>
								</div>
								<div class="media-body">
									<h4 class="media-heading">Tope Oni <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria</a>
                                </h4>

									<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus...</p>

									<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>

								</div>
								<hr class="no-margin">
							</div>

							<div class="media">
								<div class="media-left">
									<a href="#">
										<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
									</a>
								</div>
								<div class="media-body">
									<h4 class="media-heading">Emmanuel Olamide Oni <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria, and the whole of Europe</a>
                                </h4>

									<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus...</p>

									<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>

								</div>
								<hr class="no-margin">
							</div>

							<div class="media">
								<br>
								<div class="media-left">
									<a href="#">
										<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
									</a>
								</div>

								<div class="media-body">
									<h4 class="media-heading">Bolatito Ogunmodede <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria</a>
                                </h4>

									<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra...</p>

									<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>


								</div>
								<hr class="no-margin">
							</div>
						</div>

					</div>
				</div>
			</div>



		</div>
	</div>
</section>



@include ('includes.footer') @stop @section('script')

<script>
	$(document).ready(function () {

				$("#owl-demo").owlCarousel({
					items: 4,
					navigation: false,
					navigationText: ["<", ">"]
				});
</script>

@endsection