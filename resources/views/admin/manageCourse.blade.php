@extends('layout.secondary-admin') @section('content')
<header class="vla-bg-header vla-header">
	<div class="container-fluid">
		<h2 class="text-white col-sm-8 no-margin no-padding">Manage Course</h2>
		<div class="btn-quick-actions no-margin">
			<div class="col-sm-4">
				<a href="addCourse" type="button" class="btn btn-default btn-block"><i class="fa fa-book"></i>&nbsp; Add New Course</a>
			</div>
		</div>
	</div>
</header>

<section class="manager-dash manage-course bg-grey">
	<div class="container-fluid">
		<div class="well">
			<div class="row">
				<div class="col-sm-9 animated slideInLeft">
					<table class="table table-responsive col-sm-12">
						<thead>
							<tr>
								<td align="right">
									<select class="form-control" style="width:200px; display:inline">
										<option value="">Bulk Actions</option>
										<option value="">Delete</option>
									</select>
									<button class="btn btn-default">Apply</button>
								</td>
								<td class="mng-checkbox">
									<input type="checkbox" id="checkbox-1" class="regular-checkbox">
									<label for="checkbox-1"></label>
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div class="row">
										<div class="col-sm-3">
											<img src="{{asset('assets/img/week1.jpg')}}" alt="" height="100px" width="100%">
										</div>
										<div class="col-sm-9">
											<h4>Business Essentials for Professionals <small>&#45; Finance and Money</small></h4>
												<p>121 Staff Enrolled &#8226; 458 Completions</p>
												<small>Published on August 14th, 2016</small>	
											
											
											<div class="">
												<a href="" class="btn btn-default"> <i class="fa fa-edit"></i>&nbsp;Edit</a>
												<a href="" class="btn btn-default" data-toggle="modal" data-target="#delete-staff"><i class="fa fa-trash"></i>&nbsp;Delete</a>
											</div>
										</div>
									</div>
								</td>
								<td class="mng-checkbox">
									<input type="checkbox" id="checkbox-1-1" class="regular-checkbox">
									<label for="checkbox-1-1"></label>
								</td>
							</tr>
							
							<tr>
							<td>
								<div class="row">
									<div class="col-sm-3">
										<img src="{{asset('assets/img/week2.jpg')}}" alt="" height="100px">
									</div>
									<div class="col-sm-9">
										<h4>becoming a better you<small> &#45; sociology</small></h4>
												<p>29 Staff Enrolled &#8226; 76 Completions</p>
												<small>Published on May 5th, 2016</small>
										<div class="">
											<a href="" class="btn btn-default"> <i class="fa fa-edit"></i>&nbsp;Edit</a>
											<a href="" class="btn btn-default" data-toggle="modal" data-target="#delete-staff"><i class="fa fa-trash"></i>&nbsp;Delete</a>
										</div>
									</div>
								</div>
								</td>
								<td class="mng-checkbox">
									<input type="checkbox" id="checkbox-1-2" class="regular-checkbox">
									<label for="checkbox-1-2"></label>
								</td>
							</tr>
							
							<tr>
								<td>
									<div class="row">
										<div class="col-sm-3">
											<img src="{{asset('assets/img/week3.jpg')}}" alt="" height="100px" width="100%">
										</div>
										<div class="col-sm-9">
											<h4>task management 101 <small>&#45; organisation skills </small></h4>
												<p>13 Staff Enrolled &#8226; 93 Completions</p>
												<small>Published on March 28th, 2016</small>	
											
											
											<div class="">
												<a href="" class="btn btn-default"> <i class="fa fa-edit"></i>&nbsp;Edit</a>
												<a href="" class="btn btn-default" data-toggle="modal" data-target="#delete-staff"><i class="fa fa-trash"></i>&nbsp;Delete</a>
											</div>
										</div>
									</div>
								</td>
								<td class="mng-checkbox">
									<input type="checkbox" id="checkbox-1-3" class="regular-checkbox">
									<label for="checkbox-1-3"></label>
								</td>
							</tr>
							<tr>
							<td>
								<div class="row">
									<div class="col-sm-3">
										<img src="{{asset('assets/img/week1.jpg')}}" alt="" height="100px">
									</div>
									<div class="col-sm-9">
										<h4>work and life balance<small> &#45; sociology</small></h4>
												<p>88 Staff Enrolled &#8226; 76 Completions</p>
												<small>Published on January 5th, 2016</small>
										<div class="">
											<a href="" class="btn btn-default"> <i class="fa fa-edit"></i>&nbsp;Edit</a>
											<a href="" class="btn btn-default" data-toggle="modal" data-target="#delete-staff"><i class="fa fa-trash"></i>&nbsp;Delete</a>
										</div>
									</div>
								</div>
								</td>
								<td class="mng-checkbox">
									<input type="checkbox" id="checkbox-1-4" class="regular-checkbox">
									<label for="checkbox-1-4"></label>
								</td>
							</tr>
						</tbody>
					</table>
					<table class="table table-responsive col-sm-12">
						<tr>
							<td>
								<div class="form-inline">
									<ul class="pagination">
										<li><a href="#" aria-label="First">First</a>
											<li><a href="#" aria-label="Previous">Previous</a>
											</li>
											<li class="active"><a href="#">1</a></li>
											<li><a href="#" aria-label="Next">Next</a>
												<li><a href="#" aria-label="Last">Last</a>
									</ul>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-3 animated slideInRight">
					<div class="well vla-bg-black">
						<div class="filter-section">
							<label for=""><i class="fa fa-search"></i>&nbsp; Search</label>
							<div class="input-group">
								<input type="text" class="form-control no-bod-rad" placeholder="Enter keyword">
								<span class="input-group-btn">
									<button class="btn btn.default"><i class="fa fa-search"></i></button>
								</span>
							</div>
						</div>
						<div class="filter-section">
							<label for=""><i class="fa fa-list"></i>&nbsp; Categories</label>
							<select name="" id="" class="form-control">
								<option value="">Select Category</option>
								<option value="">Category 1</option>
								<option value="">Category 2</option>
								<option value="">Category 3</option>
								<option value="">Category 4</option>
							</select>
						</div>
						<div class="filter-section">
							<label for=""><i class="fa fa-building-o"></i>&nbsp; Company</label>
							<select name="" id="" class="form-control">
								<option value="">Select Company</option>
								<option value="">GT Bank</option>
								<option value="">Dangote Group</option>
								<option value="">British American Tobacco</option>
								<option value="">Microsoft Nigeria</option>
								<option value="">Konga</option>
								<option value="">UAC Foods</option>
							</select>
						</div>

						<br>
						<div class="filter-section">
							<button class="btn btn-block btn-success btn-lg">Search</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--Modal-->
<div class="modal fade" id="delete-staff" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" arial-labelledby="confirm-delete">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="confirm-delete">Delete this Staff?</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete this Staff's record permantely</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
			</div>
		</div>
	</div>
</div>

@include ('includes.footer') @stop @section('script') @endsection