@extends('layout.secondary') @section('content')

<!-- Page Content -->

<header class="course-header dash no-bg-img">
	<div class="container">
		<div class="row">
			<h1 class="tagline text-bold text-shadow-xs text-center">VLA Library</h1>
			<p class="lead no-margin text-center">
				<span class="fa fa-university"></span> 15 Categories &nbsp; &middot; &nbsp;
				<span class="fa fa-list"></span> 200 Courses &nbsp; &middot; &nbsp;
				<span class="fa fa-user"></span> 21 Speakers
			</p>
		</div>
	</div>
</header>

<!-- Page Content -->
<section class="library-body">
	<div class="container">

		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
				<div>

					<!-- Nav tabs -->
					<div class="row">
						<div class="btn-group btn-group-justified" role="group">
							<div class="btn-group" role="group" class="active">
								<a class="btn btn-default on btn-l no-bod-rad library-body" type="button" href="#library" aria-controls="home" data-toggle="tab">
									<i class="fa fa-th fa-lg"></i><span class="hidden-xs"> &nbsp; All Courses</span></a>
							</div>

							<div class="btn-group" role="group">
								<a class="btn btn-default btn-l no-bod-rad library-body" type="button" href="categories">
									<i class="fa fa-list fa-lg"></i><span class="hidden-xs"> &nbsp; Categories</span></a>
							</div>

						</div>
					</div>


					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="all-courses">
							<div class="panel-group">

								<div class="row">
									<div class="col-md-4 col-sm-6 animated zoomIn">
										<div class="panel panel-default course-item">
											<a href="course2" class="text-center course-link">
												<img src="{{asset('assets/img/week1.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
												<h4>Successful Negotiation</h4>
												<hr class="text-center course-underline">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
												<br>
												<span class="text-center col-xs-12">
												<button class="btn btn-success">View Course</button>	
											</span>
												<br>
												<br>
											</a>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 animated zoomIn">
										<div class="panel panel-default course-item">
											<a href="course2" class="text-center course-link">
												<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
												<h4>Positive Psychology</h4>
												<hr class="text-center course-underline">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
												<br>
												<span class="text-center col-xs-12">
											<button class="btn btn-success ">View Course</button>	
											</span>
												<br>
												<br>
											</a>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 animated zoomIn">
										<div class="panel panel-default course-item">
											<a href="course2" class="text-center course-link">
												<img src="{{asset('assets/img/week3.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
												<h4>Business Foundations</h4>
												<hr class="text-center course-underline">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
												<br>
												<span class="text-center col-xs-12">
											<button class="btn btn-success ">View Course</button>	
											</span>
												<br>
												<br>
											</a>
										</div>
									</div>


									<div class="col-md-4 col-sm-6 animated zoomIn">
										<div class="panel panel-default course-item">
											<a href="course2" class="text-center course-link">
												<img src="{{asset('assets/img/week1.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
												<h4>Successful Negotiation</h4>
												<hr class="text-center course-underline">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
												<br>
												<span class="text-center col-xs-12">
											<button class="btn btn-success ">View Course</button>	
											</span>
												<br>
												<br>
											</a>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 animated zoomIn">
										<div class="panel panel-default course-item">
											<a href="course2" class="text-center course-link">
												<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
												<h4>Positive Psychology</h4>
												<hr class="text-center course-underline">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
												<br>
												<span class="text-center col-xs-12">
											<button class="btn btn-success ">View Course</button>	
											</span>
												<br>
												<br>
											</a>
										</div>
									</div>

									<div class="col-md-4 col-sm-6 animated zoomIn">
										<div class="panel panel-default course-item">
											<a href="course2" class="text-center course-link">
												<img src="{{asset('assets/img/week3.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
												<h4>Business Foundations</h4>
												<hr class="text-center course-underline">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
												<br>
												<span class="text-center col-xs-12">
											<button class="btn btn-success ">View Course</button>	
											</span>
												<br>
												<br>
											</a>
										</div>
									</div>



								</div>

							</div>
						</div>
					</div>


				</div>

			</div>
		</div>
		<!-- /.row -->





		<!-- Footer -->

	</div>
</section>


@stop @section('script')

<script>
	$(document).ready(function () {

		$("#owl-demo").owlCarousel({
			items: 4
		});

		//  $('.link').on('click', function(event){
		//    var $this = $(this);
		//    if($this.hasClass('clicked')){
		//      $this.removeAttr('style').removeClass('clicked');
		//    } else{
		//      $this.css('background','#7fc242').addClass('clicked');
		//    }
		//  });

	});
</script>

@endsection
<!-- /.container -->