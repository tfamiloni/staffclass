@extends('layout.secondary') @section('content')
<section class="manager-dash bg-medium-grey">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-offset-2 col-sm-8">

				<div class="test-header vla-dark animated fadeInDown">
					<h3 class="text-center text-white text-uppercase space-lg no-margin">Module 1: Test</h3>
				</div>
				<!-- Main Section -->
				<div class="well animated fadeInUp">
					<div class="container-fluid">
						<div class="row">
							<!--						Test Information-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">Test Settings</h4>
								</div>
								<div class="panel-body">
									<h5>Test Instructions</h5>
									<textarea type="text" class="form-control" placeholder="Enter Instructions for the test..." rows="4"></textarea>
									<br>
									<div class="col-sm-3">
										<h5>No of Questions</h5>
										<input type="number" class="form-control no-bod-rad">
									</div>
									<div class="col-sm-4">
										<h5>Format</h5>
										<div class="btn-group no-margin radio-btn" data-toggle="buttons">
											<label for="" class="btn btn-default">
												<input type="radio" name="" id="" autocomplete="off">Quiz
											</label>
											<label for="" class="btn btn-default">
												<input type="radio" name="" id="" autocomplete="off">Exam
											</label>
										</div>
									</div>
									<div class="col-sm-5">
										<h5>Test Type</h5>
										<div class="btn-group no-margin radio-btn" data-toggle="buttons">
											<label for="" class="btn btn-default">
												<input type="radio" name="multiple choice" id="" autocomplete="off">Multiple choice
											</label>
											<label for="" class="btn btn-default">
												<input type="radio" name="theory" id="" autocomplete="off">Theory
											</label>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<!--Questions section-->
							<div>
								<h4>Questions</h4>
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel panel-default">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
											<div class="panel-heading" role="tab" id="question1">
												<h4 class="panel-title">Question 1<i class="pull-right fa fa-caret-down"></i></h4>
											</div>
										</a>
										<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="question1">
											<div class="panel-body">
												<h5>Question Details</h5>
												<textarea type="text" class="form-control" rows="3"></textarea>
												<hr>
												<h5>Answers</h5>
												<textarea type="text" class="form-control" rows="4"></textarea>
											</div>

										</div>
									</div>


									<div class="panel panel-default">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
											<div class="panel-heading" role="tab" id="question2">
												<h4 class="panel-title">Question 2 <i class="fa fa-caret-down pull-right"></i></h4>
											</div>
										</a>
										<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question2">

											<div class="panel-body">
												<h5>Question Details</h5>
												<textarea type="text" class="form-control" rows="3"></textarea>
												<hr>
												<h5>Options</h5>
												<div class="input-group">
													<span class="input-group-addon">
														<input type="checkbox">
													</span>
													<input type="text" class="form-control">
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon">
														<input type="checkbox">
													</span>
													<input type="text" class="form-control">
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon">
														<input type="checkbox">
													</span>
													<input type="text" class="form-control">
												</div>
												<br>
												<div class="input-group">
													<span class="input-group-addon">
														<input type="checkbox">
													</span>
													<input type="text" class="form-control">
												</div>
												
											</div>

										</div>
									</div>

									
								</div>

							</div>
							<div class="separator separator-xs"></div>
							<!--Submit Button-->
							<div id="submit">
								<a href="../module" class="btn btn-success pull-left hidden-xs" type="submit">Save & go to Module</a>
								<a href="" class="btn btn-success pull-right" type="submit">Save & Continue </a>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


@stop @section('script')

<script>
	$(document).ready(function () {

		$("#sortable").sortable();
		$("#sortable").disableSelection();
	});
</script>



@include ('includes.footer') @endsection