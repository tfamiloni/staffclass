@extends('layout.secondary') @section('content')

<section class="manager-dash bg-medium-grey">
	<div class="container-fluid">
		<div class="row">

			<!--
			<div class="col-sm-3 manager-dash-sidebar bg-dark-grey  animated slideInLeft">
				<br>
				<br>
				<h4 class="no-marin text-center text-uppercase text-muted text-bradon text-white space-lg">
				<i class="fa fa-tachometer fa-lg"></i><br><br> Dashboard
				</h4>
				<hr class="hr-xs text-center">
				<br>
				<div class="list-group list-group-module">
					<a href="addStaff" class="list-group-item">
						<span class="fa fa-user fa-lg sm-gap"></span>&nbsp; Add Staff
					</a>
					<a href="manageStaff" class="list-group-item">
						<span class="fa fa-users fa-lg sm-gap"></span>&nbsp; Manage Staff
					</a>
					<a href="addCourse" class="list-group-item active">
						<span class="fa fa-book fa-lg sm-gap"></span>&nbsp; Add Course
					</a>
					<a href="manageCourse" class="list-group-item">
						<span class="fa fa-book fa-lg sm-gap"></span>&nbsp; Manage Courses
					</a>
					<br>
					<br>
				</div>
			</div>
-->

			<div class="col-sm-offset-2 col-sm-8">
				<div class="text-white manager-header course-pattern animated fadeInDown">
					<h2 class="text-center">Add Course</h2>

				</div>
				<div class="well animated fadeInUp">
					<div class="container-fluid">
						<div class="row">
							<h4>Course Title</h4>
							<small>Lorem ipsum dolor sit amet, consectetur adipisicing.</small>
							<input type="text" placeholder="Enter Course Title Here" class="col-sm-12 col-xs-12 mng-input">
							<div class="separator-sm"></div>

							<div id="modules">
								<h4>
									Modules
									<a href="module">
									<small class="pull-right"><i class="fa fa-sm fa-plus"></i>&nbsp; Add Modules</small>
									</a>
								</h4>
								<small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, ipsa.</small>
								<div id="sortable">
									<div class="manager-panel ui-state-default">
										<p class="text-center">Module 1
											<span class="mng-btn pull-right">
										<a href="module" class="mng-edit">
											<i class="fa fa-lg fa-edit"></i>
										</a><a type="button" data-toggle="modal" data-target="#delete-module" class="mng-delete">
										<i class="fa fa-lg fa-trash"></i>
									</a>
								</span>
										</p>
									</div>
									<div class="manager-panel ui-state-default">
										<p class="text-center">Module 2
											<span class="mng-btn pull-right">
										<a href="module" class="mng-edit">
											<i class="fa fa-lg fa-edit"></i>
										</a><a type="button" data-toggle="modal" data-target="#delete-module" class="mng-delete">
										<i class="fa fa-lg fa-trash"></i>
									</a>
								</span>
										</p>
									</div>
									<div class="manager-panel ui-state-default">
										<p class="text-center">Module 3
											<span class="mng-btn pull-right">
										<a href="module" class="mng-edit">
											<i class="fa fa-lg fa-edit"></i>
										</a><a type="button" data-toggle="modal" data-target="#delete-module" class="mng-delete">
										<i class="fa fa-lg fa-trash"></i>
									</a>
								</span>
										</p>
									</div>
								</div>

							</div>

							<div class="separator-xs"></div>

							<div id="author">
								<div class="panel panel-default">
									<a data-toggle="collapse" href="#aut-collapse">
										<div class="panel-heading">
											<h4 class="panel-title">
										Author Information
										<i class="pull-right fa fa-caret-down"></i>
										</h4>
										</div>
									</a>
									<div id="aut-collapse" class="panel-collapse collapse">


										<div class="panel-body">
											<div class="input-group">
												<span class="input-group-addon">
												Author's Name
											</span>
												<input type="text" class="form-control" placeholder="Enter Author's name here">
											</div>
										</div>
										<div class="panel-body">
											<div class="input-group">
												<span class="input-group-addon">
												Position
											</span>
												<input type="text" class="form-control" placeholder="Eg. V.P. Human Resources West Africa.">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title text-center">Author's featured image</h4>
										</div>
										<div class="panel-body text-center">
										<img src="//placehold.it/200x200" alt="" width="200px" height="200px">
										<br><br>
											<a href="">
											<p><i class="fa fa-photo"></i>&nbsp; Set featured image</p>
											</a>
											
										</div>
									</div>
								</div>
					
								
								<div class="col-sm-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title text-center">Course featured image</h4>
										</div>
										<div class="panel-body text-center">
										<img src="//placehold.it/200x200" alt="" width="200px" height="200px">
										<br><br>
											<a href="">
											<p><i class="fa fa-photo"></i>&nbsp; Set featured image</p>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="separator-sm"></div>

							<div id="submit">
								<a href="dashboard" class="btn btn-success pull-left hidden-xs" type="submit">Save & go to Dashboard</a>
								<a class="btn btn-success pull-right" type="submit">Save & Continue </a>
							</div>
							<div class="separator-sm"></div>

						</div>

					</div>
				</div>

			</div>
		</div>
	</div>
</section>
<!--Modal-->
<div class="modal fade" id="delete-module" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" arial-labelledby="confirm-delete">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="confirm-delete">Delete this module?</h4>
			</div>
			<div class="modal-body">
				<p>Are you sure you want to delete this module permanetly</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">cancel</button>
			</div>
		</div>
	</div>
</div>

@stop
@section('script')

<script>
	$(document).ready(function () {

		$("#sortable").sortable();
		$("#sortable").disableSelection();
		
//		$("#delete-confirm").dialog({
//			autoOpen: false,
//			width: "60%",
//			height: "auto",
//			modal: true,
//			buttons: {
//				"Delete all items": function(){
//					$(this).dialog("close");
//				},
//				Cancel: function(){
//					$(this).dialog("close");
//				}
//			}
//			
//		});
//		$("#delete-module").on("click", function(){
//			$("#delete-confirm").dialog("open");
//		});
	});
</script>



@include ('includes.footer') 
 @endsection