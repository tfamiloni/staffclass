@extends('layout.secondary') @section('content')
<section class="manager-dash bg-grey">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-offset-2 col-sm-8">

				<div class="text-white manager-header course-pattern animated fadeInDown">
					<h2 class="text-center">Module 1</h2>
				</div>
				<!-- Main Section -->
				<div class="well animated fadeInUp">
					<div class="container-fluid">
						<div class="row">
							<!--						Module Information-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title text-center">Module Information</h4>
								</div>
								<div class="panel-body">
									<div class="input-group">
										<span class="input-group-addon">Module Title</span>
										<input type="text" class="form-control" placeholder="Enter Module Title">
									</div>
								</div>
								<div class="panel-body">
									<h5>Module Summary</h5>
									<textarea type="text" class="form-control" placeholder="Enter brief summary of the module..." rows="4"></textarea>
								</div>
							</div>
							<hr>
							<!--Lessons section-->
							<div id="lessons">
								<h4>Lessons
									<a href="">
									<small class="pull-right"><i class="fa fa-sm fa-plus"></i>&nbsp; Add Lessons</small>
									</a>
								</h4>

								<!--									lesson accordion-->
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel panel-default">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
											<div class="panel-heading" role="tab" id="lesson1">
												<h4 class="panel-title">Lesson 1<i class="pull-right fa fa-caret-down"></i></h4>
											</div>
										</a>
										<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="lesson1">
											<div class="panel-body">
												<div class="input-group">
													<span class="input-group-addon">
												Lesson Title
											</span>
													<input type="text" class="form-control" placeholder="Enter Lesson Title">
												</div>
											</div>
											<div class="panel-body">
												<h5>Lesson content</h5>
												<textarea type="text" class="form-control" rows="10"></textarea>
											</div>
											<div class="panel-body">

												<div class="row">
													<div class="col-sm-6">
														<div class="panel panel-default">
															<div class="panel-heading">
																<h4 class="text-center panel-title">Media</h4>
															</div>
															<ul class="list-group">
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
															</ul>
															<div class="panel-body text-center">
																<a href=""><i class="fa fa-upload"></i>&nbsp; Upload media content (audio or videos)</a>
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="panel panel-default">
															<div class="panel-heading">
																<h4 class="text-center panel-title">Slides</h4>
															</div>
															<ul class="list-group">
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
															</ul>
															<div class="panel-body text-center">
																<a href=""><i class="fa fa-upload"></i>&nbsp; Upload Slides</a>
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>


									<div class="panel panel-default">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
											<div class="panel-heading" role="tab" id="lesson2">
												<h4 class="panel-title">Lesson 2 <i class="fa fa-caret-down pull-right"></i></h4>
											</div>
										</a>
										<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="lesson2">
											<div class="panel-body">
												<div class="input-group">
													<span class="input-group-addon">
												Lesson Title
											</span>
													<input type="text" class="form-control" placeholder="Enter Lesson Title">
												</div>
											</div>
											<div class="panel-body">
												<h5>Lesson content</h5>
												<textarea type="text" class="form-control" rows="10"></textarea>
											</div>
											<div class="panel-body">

												<div class="row">
													<div class="col-sm-6">
														<div class="panel panel-default">
															<div class="panel-heading">
																<h4 class="text-center panel-title">Media</h4>
															</div>
															<ul class="list-group">
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
															</ul>
															<div class="panel-body text-center">
																<a href=""><i class="fa fa-upload"></i>&nbsp; Upload media content (audio or videos)</a>
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="panel panel-default">
															<div class="panel-heading">
																<h4 class="text-center panel-title">Slides</h4>
															</div>
															<ul class="list-group">
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
															</ul>
															<div class="panel-body text-center">
																<a href=""><i class="fa fa-upload"></i>&nbsp; Upload Slides</a>
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>

									<div class="panel panel-default">
										<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
											<div class="panel-heading" role="tab" id="lesson3">
												<h4 class="panel-title">Lesson 3<i class="fa fa-caret-down pull-right"></i></h4>
											</div>
										</a>
										<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="lesson3">
											<div class="panel-body">
												<div class="input-group">
													<span class="input-group-addon">
												Lesson Title
											</span>
													<input type="text" class="form-control" placeholder="Enter Lesson Title">
												</div>
											</div>
											<div class="panel-body">
												<h5>Lesson content</h5>
												<textarea type="text" class="form-control" rows="10"></textarea>
											</div>
											<div class="panel-body">

												<div class="row">
													<div class="col-sm-6">
														<div class="panel panel-default">
															<div class="panel-heading">
																<h4 class="text-center panel-title">Media</h4>
															</div>
															<ul class="list-group">
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
															</ul>
															<div class="panel-body text-center">
																<a href=""><i class="fa fa-upload"></i>&nbsp; Upload media content (audio or videos)</a>
															</div>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="panel panel-default">
															<div class="panel-heading">
																<h4 class="text-center panel-title">Slides</h4>
															</div>
															<ul class="list-group">
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
																<li class="list-group-item"></li>
															</ul>
															<div class="panel-body text-center">
																<a href=""><i class="fa fa-upload"></i>&nbsp; Upload Slides</a>
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="separator separator-xs"></div>



							<div id="test">
								<div class="manager-panel ui-state-default">
									<p class="text-center">Add a Test to this Module
										<span class="mng-btn pull-right">
										<a href="module/addTest" type="button" class="mng-edit">
											<i class="fa fa-lg fa-edit"></i>
										</a><a type="button" data-toggle="modal" data-target="#delete-test" class="mng-delete">
										<i class="fa fa-lg fa-trash"></i>
									</a>
								</span>
									</p>
								</div>

							</div>
							<div class="separator separator-xs"></div>

							<div class="row">
								<div class="col-sm-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title text-center">Module Featured Image</h4>
										</div>
										<div class="panel-body text-center">
											<img src="//placehold.it/200x200" alt="" width="200px" height="200px">
											<br>
											<br>
											<a href=""><i class="fa fa-photo"></i>&nbsp; Set featured image</a>

										</div>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title text-center">Module Resource Materials</h4>
										</div>
										<div class="panel-body text-center">
											<ul class="list-group">
												<li class="list-group-item"></li>
												<li class="list-group-item"></li>
												<li class="list-group-item"></li>
											</ul>
											<a href=""><i class="fa fa-file-archive-o"></i>&nbsp; upload resource materials</a>
										</div>
									</div>
								</div>
							</div>
							<!--Submit Button-->
							<div id="submit">
								<a href="dashboard" class="btn btn-success pull-left hidden-xs" type="submit">Save & go to Dashboard</a>
								<a class="btn btn-success pull-right" type="submit">Save & Continue </a>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


@stop @section('script')

<script>
	$(document).ready(function () {

		$("#sortable").sortable();
		$("#sortable").disableSelection();
	});
</script>



@include ('includes.footer') @endsection