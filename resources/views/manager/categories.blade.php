@extends('layout.secondary') @section('content')

<!-- Page Content -->

<header class="course-header dash no-bg-img">
	<div class="container">
		<div class="row">
			<h1 class="tagline text-bold text-shadow-xs text-center">VLA Library</h1>
			<p class="lead no-margin text-center">
				<span class="fa fa-university"></span> 15 Categories &nbsp; &middot; &nbsp;
				<span class="fa fa-list"></span> 200 Courses &nbsp; &middot; &nbsp;
				<span class="fa fa-user"></span> 21 Speakers
			</p>
		</div>
	</div>
</header>

<!-- Page Content -->
<section class="library-body">
<div class="container">

	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
			<div>

				<!-- Nav tabs -->
				<div class="row">
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group" role="group">
							<a class="btn btn-default btn-l no-bod-rad library-body" type="button" href="library">
								<i class="fa fa-th fa-lg"></i><span class="hidden-xs"> &nbsp; All Courses</span></a>
						</div>

						<div class="btn-group" role="group" class="active">
							<a class="btn btn-default on btn-l no-bod-rad library-body" type="button" href="#categories" aria-controls="home" data-toggle="tab">
								<i class="fa fa-list fa-lg"></i><span class="hidden-xs"> &nbsp; Categories</span></a>
						</div>

					</div>
				</div>


				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="all-courses">
						<div class="panel-group">

							<a href="#">
								<div class="panel panel-default panel-course category-item animated slideInLeft">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week1.jpg')}}" alt="" class="category-icon" height="90px" width="auto">	
										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Personal Development <small> &nbsp; 12 courses</small> </h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>
							
							<a href="#">
								<div class="panel panel-default panel-course category-item animated slideInRight">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="category-icon" height="90px" width="auto">	
										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Language Learning <small>&nbsp; 6 courses</small></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>
							
							<a href="#">
								<div class="panel panel-default panel-course category-item">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week3.jpg')}}" alt="" class="category-icon" height="90px" width="auto">

										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Business and Social Sciences <small>&nbsp; 25 courses</small></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>

							
							<a href="#">
								<div class="panel panel-default panel-course category-item">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week1.jpg')}}" alt="" class="category-icon" height="90px" width="auto">	
										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Personal Development <small> &nbsp; 12 courses</small> </h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>
							
							<a href="#">
								<div class="panel panel-default panel-course category-item">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="category-icon" height="90px" width="auto">	
										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Language Learning <small>&nbsp; 6 courses</small></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>
							
							<a href="#">
								<div class="panel panel-default panel-course category-item">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week3.jpg')}}" alt="" class="category-icon" height="90px" width="auto">	
										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Business and Social Sciences <small>&nbsp; 25 courses</small></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>


						</div>
					</div>
				</div>


			</div>

		</div>
	</div>
	<!-- /.row -->





	<!-- Footer -->

</div>
</section>


@include ('includes.footer') @stop @section('script')

<script>
	$(document).ready(function () {

		$("#owl-demo").owlCarousel({
			items: 4
		});

		//  $('.link').on('click', function(event){
		//    var $this = $(this);
		//    if($this.hasClass('clicked')){
		//      $this.removeAttr('style').removeClass('clicked');
		//    } else{
		//      $this.css('background','#7fc242').addClass('clicked');
		//    }
		//  });

	});
</script>

@endsection
<!-- /.container -->



