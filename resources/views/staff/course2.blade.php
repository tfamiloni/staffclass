@extends('layout.main')

@section('content')


<!-- Page Content -->
@include ('staff.includes.progress-section')  
               
    <!-- Nav tabs -->
    <div class="animated fadeIn">
<!--
      <div id="btn-nav" class="btn-group btn-group-justified" role="group">
        <div class="btn-group" role="group">
            <a class="btn on btn-default btn-l no-bod-rad" type="button" href="course2">
            <i class="fa fa-folder fa-lg"></i><span class="hidden-xs"> &nbsp; Module Outline</span></a>
        </div>
        <div class="btn-group"role="group" class="active">
            <a class="btn btn-default btn-l no-bod-rad" type="button" href="course-discussion">
            <i class="fa fa-commenting fa-lg"></i><span class="hidden-xs"> &nbsp; Discussions</span></a>
        </div>
        <div class="btn-group" role="group">
            <a class="btn btn-default btn-l no-bod-rad" type="button" href="course-materials">
            <i class="fa fa-file-archive-o fa-lg"></i><span class="hidden-xs"> &nbsp; Module Materials</span></a>
        </div>
      </div>
-->
      
      <ul class="nav nav-tabs nav-justified" role="tablist">
      	<li role="presentation" class="active">
      		<a href="#courseOutline" aria-controls="courseOutline" role="tab" data-toggle="tab"><i class="fa fa-folder fa-lg"></i><span class="hidden-xs"> &nbsp; Module Outline</span></a>
      	</li>
      	<li role="presentation">
      		<a href="#discussion" aria-controls="discussion" role="tab" data-toggle="tab"><i class="fa fa-commenting fa-lg"></i><span class="hidden-xs"> &nbsp; Discussions</span></a>
      	</li>
      	<li role="presentation">
      		<a href="#courseMaterials" aria-controls="courseMaterials" role="tab" data-toggle="tab"><i class="fa fa-file-archive fa-lg"></i><span class="hidden-xs"> &nbsp; Module Materials</span></a>
      	</li>
      </ul>
      
    </div>
  
  
  
<!--  Tab Panes-->
    <div class="tab-content animated animated-fast fadeInUp tab-home">
      <div role="tabpanel" class="tab-pane animated fadeIn active" id="courseOutline">
        <div class="container">

        <br>

        <div class="row">
            
        <div class="col-sm-12 col-md-7 col-md-offset-1">
                                                               
                      <p>
                        <span class="fa-stack">
                          <i class="fa fa-circle fa-stack-2x text-green-deep"></i>
                          <i class="fa fa-check fa-stack-1x fa-inverse"></i>
                        </span>
                        <span class="text-green-deep">You have completed this module</span>
                      </p>
                      

<!--
                    <h2 class="">
                      Week 1
                    </h2>
-->

                    <hr>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default panel-course selected">
                        <div class="panel-heading" role="tab" id="headingTwo">
                          <h4 class="panel-title">
                            <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                              <div class="row">
                                  <div class="col-sm-4">
                                   <!--  <span class="fa-stack fa-2x fa-abs-x" title="You have marked this week as completed">
                                      <i class="fa fa-circle fa-stack-2x text-green-deep"></i>
                                      <i class="fa fa-check fa-stack-1x fa-inverse"></i>
                                    </span> -->
                                    <img src="{{asset('assets/img/week1.jpg')}}" alt="" class="img-cos" width="100%"><br>
                                </div>
                                <div class="col-sm-8">
                                    <h3 class=""><span class="text-green-deep">Module 1 →</span> Discover Yourself and Understand Your Prospective Employer</h3>
                                    	<p>Getting a job is a job in itself. Competition is thick and opportunities can be scarce. To be successful in your job search, you need to identify what your prospective employer wants and find a way to offer that  value they desperately need. This is what we'll teach you in this course.

</p>
<!--                                      <small class="dotted-label">&rarr; 6 lessons &middot; 5mins </small>-->
                                </div>
                              </div>
                            </a>
                          </h4><br>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="true">
                    <h5 class="text-brandon text-uppercase space-lg pull-left">6 Lessons</h5>
                      <!--                          <a class="btn btn-default pull-right" href="class-video.php">Review Class &raquo;</a>
                          -->
                        <div class="clearfix"></div>
                          <div class="panel-body">
                              <table class="table table-course table-striped table-border table-hover">  
                              <tbody> 

                                                                  
                                  <tr> 
                                      <th scope="row" class="text-center">1</th> 
                                      <td>
                                                                             <i class="text-muted fa fa-video-camera"></i> &nbsp;
                                                                            
                                       <a href="class-video">Lesson 1: Introduction to the Module &amp; Understanding Job search in Nigeria</a>
                                       </td>
                                                                            <td class="text-center"> 
<!--                                      <span class="text-muted small">2min</span> -->
                                      <i title="You have completed this lesson. Process to next" class="fa fa-check text-success"></i></td> 
                                                                        </tr> 
                                
                                  
                                  <tr> 
                                      <th scope="row" class="text-center">2</th> 
                                      <td>
                                                                             <i class="text-muted fa fa-headphones"></i> &nbsp;
                                                                            
                                       <a href="class-article">Lesson 2: Job Search is Value Exchange</a>
                                       </td>
                                                                            <td class="text-center"> 
<!--                                      <span class="text-muted small">2min</span> -->
                                      <i title="You have completed this lesson. Process to next" class="fa fa-check text-success"></i></td> 
                                                                        </tr> 
                                
                                  
                                  <tr> 
                                      <th scope="row" class="text-center">3</th> 
                                      <td>
                                                                             <i class="text-muted fa fa-headphones"></i> &nbsp;
                                                                            
                                       <a href="class-article">Lesson 3 :  Getting a job is a job; how to be well prepared for the journey</a>
                                       </td>
                                                                            <td class="text-center"> 
<!--                                      <span class="text-muted small">2min</span> -->
                                      <i title="You have completed this lesson. Process to next" class="fa fa-check text-success"></i></td> 
                                                                        </tr> 
                                
                                  
                                  <tr> 
                                      <th scope="row" class="text-center">4</th> 
                                      <td>
                                                                             <i class="text-muted fa fa-book"></i> &nbsp;
                                                                            
                                       <a href="class-test">Short Test</a>
                                       </td>
                                                                            <td class="text-center"> 
<!--                                      <span class="text-muted small">2min</span> -->
                                      <i title="You have completed this lesson. Process to next" class="fa fa-check text-success"></i></td> 
                                                                        </tr> 
                                
                                  
                                  <tr> 
                                      <th scope="row" class="text-center">5</th> 
                                      <td>
                                                                             <i class="text-muted fa fa-video-camera"></i> &nbsp;
                                                                            
                                       <a href="class-video">Lesson 5 : Know Yourself. Take the self-awareness test.</a>
                                       </td>
                                                                            <td class="text-center"> 
<!--                                      <span class="text-muted small">2min</span> -->
                                      <i title="You have completed this lesson. Process to next" class="fa fa-check text-success"></i></td> 
                                                                        </tr> 
                                
                                  
                                  <tr> 
                                      <th scope="row" class="text-center">6</th> 
                                      <td>
                                                                             <i class="text-muted fa fa-headphones"></i> &nbsp;
                                                                            
                                       <a href="class-result">Short Test I</a>
                                       </td>
                                                                            <td class="text-center"> 
<!--                                      <span class="text-muted small">2min</span> -->
                                      <i title="You have completed this lesson. Process to next" class="fa fa-check text-success"></i></td> 
                                                                        </tr> 
                                
                                                                  
                              </tbody> 
                              </table>
                        </div>                   
                        </div>
                      </div>
                      <!-- <div class="separator separator-sm"></div> --><br>
                    </div>
                
                        </div>
            
        <div class="col-md-3 col-sm-12">
           
          

         <h5 class="text-green-deep text-center text-brandon text-uppercase space-lg">facilitator</h5><hr class="hr-xs">
            <div class="col-xs-5 col-sm-3 col-md-12 ">
                <div class="square-fixer text-center">
                  <img width="75%" class="img-circle" src="{{asset('assets/img/olushola.jpg')}}" alt="">
                </div>
                    <br>
            </div>
            <div class="col-xs-7 col-sm-9 col-md-12 center-text" id="">
                    <h5 class="no-margin">Victor Adebayo</h5>
                    <small> V.P. Human Resources West Africa, FBN Capital, G.E &amp; Lead Consultant at MegaBox Solutions &amp; Professional Services</small>
                    <br><br>
                <!-- <p class="small text-muted text-justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis praesentium eaque fugiat doloremque dignissimos, eos vitae rerum labore quibusdam, autem accusantium ad ex, reiciendis ab nostrum quia ratione. Eos, ipsam!</p> -->
            
            </div>
            <div class="separator separator-sm clearfix"><br>
            </div>
           <div class="col-sm-6 col-sm-offset-3 col-md-offset-0 col-md-12 ">
<!--
              <div class="resources">
              <h5 class="text-green text-center text-brandon text-uppercase space-lg heading">resources</h5>
                <div class="list-group ">
                                    <a href="/Applications/XAMPP/htdocs/learning-backend/public/uploads/files/1470832216-module1Lesson4card2.jpg" class="list-group-item no-border pad-sm">» module1Lesson4card2.jpg <i class="fa fa-download pull-right"></i></a>
                                    <a href="/Applications/XAMPP/htdocs/learning-backend/public/uploads/files/1470834103-module1Lesson4card2.jpg" class="list-group-item no-border pad-sm">» module1Lesson4card2.jpg <i class="fa fa-download pull-right"></i></a>
                                    <a href="/Applications/XAMPP/htdocs/learning-backend/public/uploads/files/1470835202-module1Lesson4card1.jpg" class="list-group-item no-border pad-sm">» Moduleer is the Way <i class="fa fa-download pull-right"></i></a>
                                    <a href="/Applications/XAMPP/htdocs/learning-backend/public_html/uploads/files/1470916447-NGO’s in Nigeria and How to Find Volunteer Roles Module 1.pdf" class="list-group-item no-border pad-sm">» NGO’s in Nigeria and How to Find Volunteer Roles <i class="fa fa-download pull-right"></i></a>
                                  </div>
              </div>
-->
              <div class="clearfix"></div>
              <div class="separator separator-sm clearfix"><br>
            </div>
            </div>

        </div>

        </div>
        <!-- /.row -->


            

                <div class="row">
            </div>
        <!-- Footer -->

    </div>
			</div>
			
<!--			Tab 2-->
			<div role="tabpanel" class="tab-pane animated fadeIn" id="discussion">
				<div class="container">

        <br>

        <div class="row">
            
        <div class="col-sm-12 col-md-8 col-md-offset-2">
               
               <div id="comment-section">
         <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="home">
                  <h3 class=" text-center">Module 1 Course Discussions</h3>

                <div id="" class="comment">
<!--                    <h5 class="text-brandon text-uppercase text-green-deep text-center">7 comments so far</h5>-->
                    <hr>
                    
                    <div class="media hidden"> 
                    
                        <div class="media-left"> 
                            <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/300x300/ffffff/17ba2a&amp;text=AB"> 
                            </a> 
                        </div> 
                    
                        <div class="media-body"> 
                            <h4 class="media-heading">Media heading
                            </h4> 
                    
                            <span><span class="comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</span> </span>
                            
                            <div class="reply-sec">
                                <p style="margin-top:7px">
                                    <a class="small" role="button" data-toggle="collapse" href="#replyForm" aria-expanded="false" aria-controls="replyForm"><i class="fa fa-commenting"></i> Reply to comment</a>
                                </p>
                                <div class="collapse" id="replyForm">
                                    <form action="">
                                        <textarea name="" id="" class="form-control form-group"></textarea>
                                        <input type="submit" class="btn btn-sm" value="Comment"><hr>
                                    </form>
                                </div>
                            </div>
                    
                            <div class="media"> 
                                <div class="media-left"> 
                                    <a href="#"> 
                                        <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM2ZmQ2NyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzZmZDY3Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;"> 
                                        </a> 
                                    </div> 
                                <div class="media-body">
                                    <h4 class="media-heading">Nested media heading</h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus. 
                                </div> <br>
                            </div>
                    
                            <div class="media"> 
                                <div class="media-left"> 
                                    <a href="#"> 
                                        <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM2ZmQ2NyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzZmZDY3Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;"> 
                                        </a> 
                                    </div> 
                                <div class="media-body">
                                    <h4 class="media-heading">Nested media heading</h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus. 
                    
                    
                                </div> <br>
                            </div>
                            
                        </div> 
                        <br>
                    </div>
                    

                    

                   
                    <div class="media" id="comment-section"> 
                        <div class="media-left"> 
                        <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/000000&amp;text=OA"> 
                        <!-- <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">  -->
                        </a> 
                        </div> 
                        <div class="media-body"> 
                            <h4 class="media-heading">Oshunlana Ayodele <small>made a comment</small> <br>
                            <small>LESSON 10: HOW TO IMPRESS FOR SUCCESS WITH EMAIL AND PHONE-CALL FOLLOW UP</small>
                            </h4> I like this 
                            
                                    <div class="reply-sec">
                                        <p style="margin-top:7px">
                                            <a class="small" role="button" data-toggle="collapse" href="#replyForm1" aria-expanded="false" aria-controls="replyForm2"><i class="fa fa-commenting"></i> Reply to comment</a>
                                        </p>
                                        <div class="collapse" id="replyForm1">
                                            <form id="ReplyComment1" action="http://learnn.insidify.com/ajax-comment" method="post">
                                                 <input type="hidden" name="_token" value="LhRblEibBIwD0vnXvQWGliEcboBf0AShqeYDfMd0">
                                                
                                                <input type="hidden" name="comment_target" value="COMMENT">
                                                <input type="hidden" name="comment_target_id" value="1">

                                                <textarea name="comment" id="" class="form-control form-group" required=""></textarea>
                                                <input type="submit" id="SubmitReply1" class="btn btn-sm" value="Comment"><hr>
                                            </form>
                                        </div>
                                    </div>
                                                                        
                                                                             <div class="media"> 
                                            <div class="media-left"> 
                                                <a href="#"> 
                                                     <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/000000&amp;text=OA"> 
                                                    </a> 
                                                </div> 
                                            <div class="media-body">
                                                <h4 class="media-heading">Oshunlana Ayodele</h4> What do you like about it?
                                            </div> <br>
                                        </div>
                                                                                                 </div> 
                    </div>
      
                    <div class="media" id="comment-section"> 
                        <div class="media-left"> 
                        <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/17ba2a&amp;text=OA"> 
                        <!-- <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">  -->
                        </a> 
                        </div> 
                        <div class="media-body"> 
                            <h4 class="media-heading">Oshunlana Ayodele <small>made a comment</small> <br>
                            <small>LESSON 2  –  HOW TO PASS APTITUDE TESTS: LEVEL 1</small>
                            </h4> Lovely class. Looking forward to the next. 
                            
                                    <div class="reply-sec">
                                        <p style="margin-top:7px">
                                            <a class="small" role="button" data-toggle="collapse" href="#replyForm3" aria-expanded="false" aria-controls="replyForm2"><i class="fa fa-commenting"></i> Reply to comment</a>
                                        </p>
                                        <div class="collapse" id="replyForm3">
                                            <form id="ReplyComment3" action="http://learnn.insidify.com/ajax-comment" method="post">
                                                 <input type="hidden" name="_token" value="LhRblEibBIwD0vnXvQWGliEcboBf0AShqeYDfMd0">
                                                
                                                <input type="hidden" name="comment_target" value="COMMENT">
                                                <input type="hidden" name="comment_target_id" value="3">

                                                <textarea name="comment" id="" class="form-control form-group" required=""></textarea>
                                                <input type="submit" id="SubmitReply3" class="btn btn-sm" value="Comment"><hr>
                                            </form>
                                        </div>
                                    </div>
                                                                                                                                   </div> 
                    </div>

                   
                    <div class="media" id="comment-section"> 
                        <div class="media-left"> 
                        <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/17ba2a&amp;text=OA"> 
                        <!-- <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">  -->
                        </a> 
                        </div> 
                        <div class="media-body"> 
                            <h4 class="media-heading">Oshunlana Ayodele <small>made a comment</small> <br>
                            <small>LESSON 1: INTRODUCTION TO THE MODULE &amp; UNDERSTANDING JOB SEARCH IN NIGERIA</small>
                            </h4> I like this. Thanks 
                            
                                    <div class="reply-sec">
                                        <p style="margin-top:7px">
                                            <a class="small" role="button" data-toggle="collapse" href="#replyForm4" aria-expanded="false" aria-controls="replyForm2"><i class="fa fa-commenting"></i> Reply to comment</a>
                                        </p>
                                        <div class="collapse" id="replyForm4">
                                            <form id="ReplyComment4" action="http://learnn.insidify.com/ajax-comment" method="post">
                                                 <input type="hidden" name="_token" value="LhRblEibBIwD0vnXvQWGliEcboBf0AShqeYDfMd0">
                                                
                                                <input type="hidden" name="comment_target" value="COMMENT">
                                                <input type="hidden" name="comment_target_id" value="4">

                                                <textarea name="comment" id="" class="form-control form-group" required=""></textarea>
                                                <input type="submit" id="SubmitReply4" class="btn btn-sm" value="Comment"><hr>
                                            </form>
                                        </div>
                                    </div>
                                                                                                                                   </div> 
                    </div>


                     

                   
                    <div class="media" id="comment-section"> 
                        <div class="media-left"> 
                        <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/17ba2a&amp;text=OA"> 
                        <!-- <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">  -->
                        </a> 
                        </div> 
                        <div class="media-body"> 
                            <h4 class="media-heading">Oshunlana Ayodele <small>made a comment</small> <br>
                            <small>LESSON 2: JOB SEARCH IS VALUE EXCHANGE</small>
                            </h4> Thanks 
                            
                                    <div class="reply-sec">
                                        <p style="margin-top:7px">
                                            <a class="small" role="button" data-toggle="collapse" href="#replyForm6" aria-expanded="false" aria-controls="replyForm2"><i class="fa fa-commenting"></i> Reply to comment</a>
                                        </p>
                                        <div class="collapse" id="replyForm6">
                                            <form id="ReplyComment6" action="http://learnn.insidify.com/ajax-comment" method="post">
                                                 <input type="hidden" name="_token" value="LhRblEibBIwD0vnXvQWGliEcboBf0AShqeYDfMd0">
                                                
                                                <input type="hidden" name="comment_target" value="COMMENT">
                                                <input type="hidden" name="comment_target_id" value="6">

                                                <textarea name="comment" id="" class="form-control form-group" required=""></textarea>
                                                <input type="submit" id="SubmitReply6" class="btn btn-sm" value="Comment"><hr>
                                            </form>
                                        </div>
                                    </div>
                                                                        
                                                                             <div class="media"> 
                                            <div class="media-left"> 
                                                <a href="#"> 
                                                     <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/17ba2a&amp;text=HO"> 
                                                    </a> 
                                                </div> 
                                            <div class="media-body">
                                                <h4 class="media-heading">Hawks Ololade </h4> Hmmmmm. Good stuff
                                            </div> <br>
                                        </div>
                                                                                                 </div> 
                    </div>


                   
                    <div class="media" id="comment-section"> 
                        <div class="media-left"> 
                        <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/000000&amp;text=OA"> 
                        <!-- <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">  -->
                        </a> 
                        </div> 
                        <div class="media-body"> 
                            <h4 class="media-heading">Oshunlana Ayodele <small>made a comment</small> <br>
                            <small>LESSON 2: UNDERSTANDING THE ROLE &amp; IMPORTANCE OF A REMARKABLE RESUME</small>
                            </h4> Thank you. I love this course 
                            
                                    <div class="reply-sec">
                                        <p style="margin-top:7px">
                                            <a class="small" role="button" data-toggle="collapse" href="#replyForm8" aria-expanded="false" aria-controls="replyForm2"><i class="fa fa-commenting"></i> Reply to comment</a>
                                        </p>
                                        <div class="collapse" id="replyForm8">
                                            <form id="ReplyComment8" action="http://learnn.insidify.com/ajax-comment" method="post">
                                                 <input type="hidden" name="_token" value="LhRblEibBIwD0vnXvQWGliEcboBf0AShqeYDfMd0">
                                                
                                                <input type="hidden" name="comment_target" value="COMMENT">
                                                <input type="hidden" name="comment_target_id" value="8">

                                                <textarea name="comment" id="" class="form-control form-group" required=""></textarea>
                                                <input type="submit" id="SubmitReply8" class="btn btn-sm" value="Comment"><hr>
                                            </form>
                                        </div>
                                    </div>
                                                                                                                                   </div> 
                    </div>

                    <div class="separator separator-xs"></div>
                                   
              </div>
              <!-- Tab1 -->



            </div>

          </div>

        </div>
                
       </div>
        </div>
            

        <!-- /.row -->


            

                <div class="row">
            </div>
        <!-- Footer -->

    </div>
			</div>
			
<!--			Tab 3-->
		<div role="tabpanel" class="tab-pane animated fadeIn" id="courseMaterials">
			<div class="container">

        <br>

       <div class="row">
              <div class="col-sm-8 col-sm-offset-2">
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="home">
                  <h3 class="">Course Materials</h3>
                    <div class="res">
                      <h5 class="text-brandon text-uppercase space-lg">Week one</h5>
                      
                      <div class="list-group">
                        <button type="button" class="list-group-item">Cras justo odio <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Dapibus ac facilisis in <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Morbi leo risus <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Porta ac consectetur ac <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Vestibulum at eros  <i class="fa fa-download pull-right"></i> </button>
                      </div>
                    </div>
                
                    <div class="res"><br>
                      <h5 class="text-brandon text-uppercase space-lg">Week two</h5>
                      
                      <div class="list-group">
                        <button type="button" class="list-group-item">Cras justo odio <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Dapibus ac facilisis in <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Morbi leo risus <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Porta ac consectetur ac <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Vestibulum at eros  <i class="fa fa-download pull-right"></i> </button>
                      </div>
                    </div>
                
                    <div class="res"><br>
                      <h5 class="text-brandon text-uppercase space-lg">Week three</h5>
                      
                      <div class="list-group">
                        <button type="button" class="list-group-item">Cras justo odio <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Dapibus ac facilisis in <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Morbi leo risus <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Porta ac consectetur ac <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Vestibulum at eros  <i class="fa fa-download pull-right"></i> </button>
                      </div>
                    </div>
                    </div>
                  <!-- Tab1 -->
                
                
                
                
                
                
                
                  <div role="tabpanel" class="tab-pane" id="profile">
                    <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                     <h5 class="text-green-deep text-center text-brandon text-uppercase space-lg">meet your professors</h5><hr class="hr-xs">
                
                        <div class="col-sm-6 col-md-3 text-center">
                            <a class="prof" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><img class="img-circle img-responsive img-center" width="75%" src="img/tewe.jpg" alt="">
                                <br>
                                <h4 class="no-margin">Lanre Olushola</h4>
                                <p> CEO, Insidify.net. Ghana</p></a>
                                <br>
                        </div>
                        <div class="col-sm-6 col-md-3 text-center">
                            <a class="prof" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><img class="img-circle img-responsive img-center" width="75%" src="img/steve.jpg" alt="">
                                <br>
                                <h4 class="no-margin">Herbert Macaulay</h4>
                                <p> CEO, Insidify.net. Ghana</p></a>
                                <br>
                        </div>
                        <div class="col-sm-6 col-md-3 text-center">
                            <a class="prof" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><img class="img-circle img-responsive img-center" width="75%" src="img/amanda.jpg" alt="">
                                <br>
                                <h4 class="no-margin">Chiamanada Oge</h4>
                                <p> CEO, Insidify.net. Ghana</p></a>
                                <br>
                        </div>
                        <div class="col-sm-6 col-md-3 text-center">
                            <a class="prof" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><img class="img-circle img-responsive img-center" width="75%" src="img/olushola.jpg" alt="">
                                <br>
                                <h4 class="no-margin">Ernest Dudu</h4>
                                <p> CEO, Insidify.net. Ghana</p></a>
                                <br>
                        </div>
                        <div class="col-xs-12 collapse" id="collapseExample">
                            <h4>Name</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis praesentium eaque fugiat doloremque dignissimos, eos vitae rerum labore quibusdam, autem accusantium ad ex, reiciendis ab nostrum quia ratione. Eos, ipsam!</p>
                        </div>
                
                    <div class="separator separator-sm clearfix"></div>
                    </div>
                      <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4"><br>
                        <a href="class-video.php" class="btn btn-lg btn-warning text-brandon text-uppercase">  &nbsp; 25% Done &nbsp; | &nbsp; Continue <span class="hidden-xs">Course</span>  &nbsp; <i class="fa fa-chevron-right"></i></a>
                      </div>
                </div>
                  </div>
                  <!-- Tab2 -->
                
                
                  <div role="tabpanel" class="tab-pane" id="messages">
                      3
                  </div>
                  <div role="tabpanel" class="tab-pane" id="settings">
                      4
                  </div>
                </div>
              </div>
            </div>
        <!-- /.row -->


            

                <div class="row">
            </div>
        <!-- Footer -->

    </div>
		</div>
			
</div>
<!--
<section class="center-text">
    <div class="container">
        <div class="row">
            More courses section
        </div>
    </div>
</section>
-->




@stop


@section('script')

<script>
    $(document).ready(function() {

      $("#owl-demo").owlCarousel({
        items : 4,
        navigation : true,
        navigationText : ["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"],
        pagination: false
      });

    //  $('.link').on('click', function(event){
    //    var $this = $(this);
    //    if($this.hasClass('clicked')){
    //      $this.removeAttr('style').removeClass('clicked');
    //    } else{
    //      $this.css('background','#7fc242').addClass('clicked');
    //    }
    //  });

    });
</script>
    
@endsection
<!-- /.container --> 