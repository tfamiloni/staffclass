@extends('layout.main') @section('content')

<section id="editProfile" class="manager-dash bg-grey">
	<div class="container-fluid">
		<div class="row">

			<div class="col-sm-10 col-sm-offset-1">
				<h2 class="text-300 text-center">Edit Profile</h2>
				<br>
				<div class="row">
					<div class="col-sm-4 no-padding">
						<div class="well staff-tab text-center no-border profile-bg animated fadeInLeft">
						<br>
							<img src="{{asset('assets/img/olushola.jpg')}}" alt="profile image" width="200px" height="200px" class="staff-avatar">
							<h2>Olufemi Badejo</h2>
<!--							<a class="btn btn-default" href=""><i class="fa fa-photo">&nbsp;</i> Change Image</a>-->
						</div>
					</div>
					<div class="col-sm-8 no-padding">
						<div class="well staff-tab animated fadeInRight">
						<div class="row">
<!--
							<div class="col-sm-12">
							<h2>Olufemi Badejo</h2>
							<hr class="no-margin">
							</div>
-->
							<div class="col-sm-6">
								<h5>Position</h5>
								<input type="text" class="mng-input col-xs-12" value="Utilities Manager">
							</div>
							<div class="col-sm-6">
								<h5>Department</h5>
								<input type="text" class="mng-input col-xs-12" value="Engineering">
							</div>
							<div class="col-sm-6">
								<h5>Company</h5>
								<input type="text" class="mng-input col-xs-12" value="British American Tobacco">
							</div>
							<div class="col-sm-6">
								<h5>Location</h5>
								<select type="text" class="form-control mng-input" style="" required="">
												<option value="1">Lagos</option>
												<option value="2">Abuja</option>
												<option value="3">Abia</option>
												<option value="4">Adamawa</option>
												<option value="5">Akwa ibom</option>
												<option value="6">Anambra</option>
												<option value="7">Bauchi</option>
												<option value="8">Bayelsa</option>
												<option value="9">Benue</option>
												<option value="10">Borno</option>
												<option value="11">Cross river</option>
												<option value="12">Delta</option>
												<option value="13">Edo</option>
												<option value="14">Ebonyi</option>
												<option value="15">Ekiti</option>
												<option value="16">Enugu</option>
												<option value="17">Gombe</option>
												<option value="18">Imo </option>
												<option value="19">Jigawa</option>
												<option value="20">Kaduna</option>
												<option value="21">Kano</option>
												<option value="22">Katsina</option>
												<option value="23">Kebbi</option>
												<option value="24">Kogi</option>
												<option value="25">Kwara</option>
												<option value="26">Niger</option>
												<option value="27">Ogun</option>
												<option value="28">Ondo</option>
												<option value="29">Osun</option>
												<option value="30">Oyo</option>
												<option value="31">Nassarawa</option>
												<option value="32">Plateau</option>
												<option value="33">Rivers</option>
												<option value="34">Sokoto</option>
												<option value="35">Taraba</option>
												<option value="36">Yobe</option>
												<option value="37">Zamfara</option>
												<option value="53">Nigeria</option>
											</select>
							</div>
							<div class="col-sm-6">
								<h5>Hired Date</h5>
								<input type="date" class="mng-input col-xs-12">
							</div>
							
							
							<div class="col-sm-6">
								<h5>Select Gender</h5>
							<div class="btn-group no-margin" data-toggle="buttons">
								<label for="" class="btn btn-default">
									<input type="radio" name="not-specified" id="" autocomplete="off">Not Specified
								</label>
								<label for="" class="btn btn-default active">
									<input type="radio" name="male" id="" autocomplete="off" checked>Male
								</label>
								<label for="" class="btn btn-default">
									<input type="radio" name="female" id="" autocomplete="off">Female
								</label>
							</div>
							</div>
							</div>
							<br>
							<div id="submit">
								<a href="my-profile" class="btn btn-success" type="submit">Save Changes</a>
							</div>
						</div>
					</div>
				</div>
	
				
			</div>
		</div>
	</div>
</section>

@stop @section('script') @endsection