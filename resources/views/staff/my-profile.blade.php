@extends('layout.main') @section('content')

<section id="myProfile" class="manager-dash bg-grey">
	<div class="container-fluid">
		<div class="row">

			<div class="col-sm-8 col-sm-offset-2">
				<h2 class="text-300">My Profile</h2>
				<br>
				<div class="row">
					<div class="col-sm-4 no-padding">
						<div class="well staff-tab text-center no-border profile-bg animated fadeInLeft">
						
							<img src="{{asset('assets/img/olushola.jpg')}}" alt="profile image" width="200px" height="200px" class="staff-avatar">
						</div>
					</div>
					<div class="col-sm-8 no-padding">
						<div class="well staff-tab animated fadeInRight">
							<h2>Olufemi Badejo</h2>
							<hr>
							<h4>Utilities Manager at British American Tobacco</h4>
							<h6>Ibadan | Engineering</h6>
							<div class="separator-xs"></div>
							<div id="submit">
								<a href="edit-profile" class="btn btn-success" type="submit">Edit Profile</a>
							</div>
						</div>
					</div>
				</div>

		
			</div>
		</div>
	</div>
</section>

@stop @section('script') @endsection