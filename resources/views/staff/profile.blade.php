@extends('layout.main') @section('content')

<section id="addStaff" class="manager-dash bg-grey">
	<div class="container-fluid">
		<div class="row">

			<div class="col-sm-10 col-sm-offset-1">
				<div class="row">	
					<h2 class="text-300 pull-left">Profile</h2><br>
				</div>
				<div class="row">
					<div class="col-sm-12 no-padding">
						<div class="course-header dash no-bg-img text-center no-border">
							<br>
							<img src="{{asset('assets/img/olushola.jpg')}}" alt="profile image" width="175px" height="175px" class="img-circle staff-avatar">
							<h3 class="text-white">Olagunju Thomas Oluwatobi</h3>
							<p>Human Resource Manager at British American Tobacco</p>
						</div>
					</div>
					<div class="col-sm-8 no-padding hide">
						<div class="well staff-tab">
							<h4>First Name</h4>
							<p>Olagunju Thomas Oluwatobi</p>

							<h4>Last Name</h4>
							<p>Another Name here</p>

							<h4>Position</h4>
							<p>Quality Assurance Agent</p>

							<div class="separator-sm"></div>
							<div id="submit">
								<a class="btn btn-default" type="submit" data-toggle="modal" data-target="#">Edit Profile</a>
							</div>
						</div>
					</div>

				</div>

				<div class="row" style="background:#fff">

					<div class="btn-group btn-group-justified" role="group">

						<div class="btn-group" role="presentation">
							<a class="btn btn-default btn-l no-bod-rad library-body on" href="#pDetails" aria-controls="Courses" role="tab" data-toggle="tab">
								<i class="fa fa-user fa-lg"></i><span class="hidden-xs"> &nbsp; Personal Details</span></a>
						</div>

						<div class="btn-group active" role="presentation">
							<a class="btn btn-default btn-l no-bod-rad library-body" href="#Activity" aria-controls="Activity" role="tab" data-toggle="tab">
								<i class="fa fa-comment-o fa-lg"></i><span class="hidden-xs"> &nbsp; Activity</span></a>
						</div>

						<div class="btn-group" role="presentation">
							<a class="btn btn-default btn-l no-bod-rad library-body" href="#Courses" aria-controls="Courses" role="tab" data-toggle="tab">
								<i class="fa fa-list fa-lg"></i><span class="hidden-xs"> &nbsp; Courses</span></a>
						</div>


					</div>
					<div class="col-sm-12">
						<disv class="tab-content">
						
							<div role="tabpanel" class="tab-pane active" id="pDetails">
								
								<div class="col-sm-6">
										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title">User Information</div>
											</div>
											<div class="panel-body panel-info">
												<div class="panel-content">
											<h5><i class="fa fa-envelope-o"></i>&nbsp;Email:</h5>
											<p>olagunju.thomas@gmail.com</p>
										</div>
										<div class="panel-content">
											<h5><i class="fa fa-phone"></i>&nbsp;Phone:</h5>
											<p>09023458901</p>
										</div>
										<div class="panel-content">
											<h5><i class="fa fa-user"></i>&nbsp;Position:</h5>
											<p>Human Resourse Manager</p>
										</div>
										<div class="panel-content">
											<h5><i class="fa fa-venus-mars"></i>&nbsp; Gender:</h5>
											<p>Male</p>
										</div>
											</div>
										</div>
									</div>
									
									
									<div class="col-sm-6">
										<div class="panel panel-default">
											<div class="panel-heading">
												<div class="panel-title">Company Information</div>
											</div>
											<div class="panel-body panel-info">
												<div class="panel-content">
											<h5><i class="fa fa-building"></i>&nbsp;Organisation:</h5>
											<p>British American Tobacco</p>
										</div>
										
										<div class="panel-content">
											<h5><i class="fa fa-calendar-o"></i>&nbsp;Hire Date:</h5>
											<p>15th May, 2009</p>
										</div>
										
										<div class="panel-content">
											<h5><i class="fa fa-building-o"></i>&nbsp; Department:</h5>
											<p>Human Resource</p>
										</div>
										<div class="panel-content">
											<h5><i class="fa fa-map-marker"></i>&nbsp;Location:</h5>
											<p>Lagos</p>
										</div>
											</div>		
										</div>
									</div>
									
									<div id="submit">
								<a href="edit-profile" class="btn btn-success" type="submit">Edit Profile</a>
							</div>
									
							</div>
							<!--Courses-->
							<div role="tabpanel" class="tab-pane" id="Courses">
								<div class="panel panel-default panel-course category-item">

									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="category-icon" height="90px" width="auto">
										</div>
										<div class="col-md-9 col-sm-9 col-xs-12">
											<h4>Language Learning <small>&nbsp; 6 courses</small></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>

											<div class="progress">
												<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
													60%
												</div>
											</div>
											<table class="table">
												<thead>
													<th>Last Seen</th>
													<th>No of Hours Spent</th>
													<th>Score</th>
													<th>Pending Assignment</th>
												</thead>
												<tbody>
													<tr>
														<td>2 hours ago</td>
														<td>13 hours </td>
														<td>75%</td>
														<td>2</td>
													</tr>
												</tbody>
											</table>
										</div>

									</div>
								</div>

								<div class="panel panel-default panel-course category-item">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="category-icon" height="90px" width="auto">
										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Language Learning <small>&nbsp; 6 courses</small></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</div>
							<!--                            Activity Panel-->
							<div role="tabpanel" class="tab-pane" id="Activity" style="padding: 0 25px 50px;">

								<div class="media">
									<br>
									<div class="media-left">
										<a href="#">
											<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
										</a>
									</div>

									<div class="media-body">
										<h4 class="media-heading">Bolatito Ogunmodede <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria</a>
                                </h4>

										<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra...</p>

										<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>


									</div>
									<hr class="no-margin">
								</div>

								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Tope Oni <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria</a>
                                </h4>

										<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus...</p>

										<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>

									</div>
									<hr class="no-margin">
								</div>

								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Emmanuel Olamide Oni <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria, and the whole of Europe</a>
                                </h4>

										<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus...</p>

										<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>

									</div>
									<hr class="no-margin">
								</div>

								<div class="media">
									<br>
									<div class="media-left">
										<a href="#">
											<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
										</a>
									</div>

									<div class="media-body">
										<h4 class="media-heading">Bolatito Ogunmodede <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria</a>
                                </h4>

										<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra...</p>

										<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>


									</div>
									<hr class="no-margin">
								</div>

							</div>


					</div>

				</div>

			</div>

			<!--				Modal-->
			<div class="modal fade" id="staffSuccess" tabindex="-1" role="dialog" aria-labelled-by="addStaffSuccess">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header no-border">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
						<div class="modal-body text-center">
							<i class="success-icon glyphicon glyphicon-ok-circle"></i>
							<h3 class="text-brandon">Staff Added Successfully</h3>
						</div>
						<div class="modal-footer no-border">
							<a href="addStaff" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-user-plus"></i>Add New Staff</a>
							<a href="" type="button" class="btn btn-default" data-dismiss="modal">Bulk Upload Staff</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	</div>
</section>

@stop @section('script') @endsection