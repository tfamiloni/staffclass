@extends('layout.main')

@section('content')


<!-- Page Content -->

@include ('staff.includes.progress-section')             
    <!-- Nav tabs -->
    <div class="animated fadeIn">
      <div class="btn-group btn-group-justified" role="group">
        <div class="btn-group" role="group">
            <a class="btn btn-default btn-l no-bod-rad" type="button" href="course2">
            <i class="fa fa-folder fa-lg"></i><span class="hidden-xs"> &nbsp; Module Outline</span></a>
        </div>
       <!--  <div class="btn-group" role="group">
            <a class="btn btn-default btn-l no-bod-rad" type="button" href="#profile" aria-controls="profile" data-toggle="tab">
            <i class="fa fa-edit fa-lg"></i><span class="hidden-xs"> &nbsp; Scores</span></a>
        </div> -->
        <div class="btn-group"role="group" class="active">
            <a class="btn btn-default btn-l no-bod-rad" type="button" href="course-discussion">
            <i class="fa fa-commenting fa-lg"></i><span class="hidden-xs"> &nbsp; Discussions</span></a>
        </div>
        <div class="btn-group" role="group">
            <a class="btn btn-default btn-l no-bod-rad on" type="button" href="course-materials">
            <i class="fa fa-file-archive-o fa-lg"></i><span class="hidden-xs"> &nbsp; Module Materials</span></a>
        </div>
      </div>
    </div>
    
    <div class="container">

        <br>

       <div class="row">
              <div class="col-sm-8 col-sm-offset-2">
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="home">
                  <h3 class="">Course Materials</h3>
                    <div class="res">
                      <h5 class="text-brandon text-uppercase space-lg">Week one</h5>
                      
                      <div class="list-group">
                        <button type="button" class="list-group-item">Cras justo odio <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Dapibus ac facilisis in <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Morbi leo risus <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Porta ac consectetur ac <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Vestibulum at eros  <i class="fa fa-download pull-right"></i> </button>
                      </div>
                    </div>
                
                    <div class="res"><br>
                      <h5 class="text-brandon text-uppercase space-lg">Week two</h5>
                      
                      <div class="list-group">
                        <button type="button" class="list-group-item">Cras justo odio <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Dapibus ac facilisis in <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Morbi leo risus <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Porta ac consectetur ac <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Vestibulum at eros  <i class="fa fa-download pull-right"></i> </button>
                      </div>
                    </div>
                
                    <div class="res"><br>
                      <h5 class="text-brandon text-uppercase space-lg">Week three</h5>
                      
                      <div class="list-group">
                        <button type="button" class="list-group-item">Cras justo odio <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Dapibus ac facilisis in <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Morbi leo risus <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Porta ac consectetur ac <i class="fa fa-download pull-right"></i> </button>
                        <button type="button" class="list-group-item">Vestibulum at eros  <i class="fa fa-download pull-right"></i> </button>
                      </div>
                    </div>
                    </div>
                  <!-- Tab1 -->
                
                
                
                
                
                
                
                  <div role="tabpanel" class="tab-pane" id="profile">
                    <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                     <h5 class="text-green-deep text-center text-brandon text-uppercase space-lg">meet your professors</h5><hr class="hr-xs">
                
                        <div class="col-sm-6 col-md-3 text-center">
                            <a class="prof" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><img class="img-circle img-responsive img-center" width="75%" src="img/tewe.jpg" alt="">
                                <br>
                                <h4 class="no-margin">Lanre Olushola</h4>
                                <p> CEO, Insidify.net. Ghana</p></a>
                                <br>
                        </div>
                        <div class="col-sm-6 col-md-3 text-center">
                            <a class="prof" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><img class="img-circle img-responsive img-center" width="75%" src="img/steve.jpg" alt="">
                                <br>
                                <h4 class="no-margin">Herbert Macaulay</h4>
                                <p> CEO, Insidify.net. Ghana</p></a>
                                <br>
                        </div>
                        <div class="col-sm-6 col-md-3 text-center">
                            <a class="prof" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><img class="img-circle img-responsive img-center" width="75%" src="img/amanda.jpg" alt="">
                                <br>
                                <h4 class="no-margin">Chiamanada Oge</h4>
                                <p> CEO, Insidify.net. Ghana</p></a>
                                <br>
                        </div>
                        <div class="col-sm-6 col-md-3 text-center">
                            <a class="prof" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><img class="img-circle img-responsive img-center" width="75%" src="img/olushola.jpg" alt="">
                                <br>
                                <h4 class="no-margin">Ernest Dudu</h4>
                                <p> CEO, Insidify.net. Ghana</p></a>
                                <br>
                        </div>
                        <div class="col-xs-12 collapse" id="collapseExample">
                            <h4>Name</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis praesentium eaque fugiat doloremque dignissimos, eos vitae rerum labore quibusdam, autem accusantium ad ex, reiciendis ab nostrum quia ratione. Eos, ipsam!</p>
                        </div>
                
                    <div class="separator separator-sm clearfix"></div>
                    </div>
                      <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4"><br>
                        <a href="class-video.php" class="btn btn-lg btn-warning text-brandon text-uppercase">  &nbsp; 25% Done &nbsp; | &nbsp; Continue <span class="hidden-xs">Course</span>  &nbsp; <i class="fa fa-chevron-right"></i></a>
                      </div>
                </div>
                  </div>
                  <!-- Tab2 -->
                
                
                  <div role="tabpanel" class="tab-pane" id="messages">
                      3
                  </div>
                  <div role="tabpanel" class="tab-pane" id="settings">
                      4
                  </div>
                </div>
              </div>
            </div>
        <!-- /.row -->


            

                <div class="row">
            </div>
        <!-- Footer -->

    </div>
    
<!--
<section class="center-text">
    <div class="container">
        <div class="row">
            More courses section
        </div>
    </div>
</section>
-->




@stop


@section('script')

<script>
    $(document).ready(function() {

      $("#owl-demo").owlCarousel({
        items : 4
      });

    //  $('.link').on('click', function(event){
    //    var $this = $(this);
    //    if($this.hasClass('clicked')){
    //      $this.removeAttr('style').removeClass('clicked');
    //    } else{
    //      $this.css('background','#7fc242').addClass('clicked');
    //    }
    //  });

    });
</script>
   
<script>
    function scrollWin() {
        window.scrollTo(0, 300);
    }
    
    scrollWin()
</script>    
@endsection
<!-- /.container --> 