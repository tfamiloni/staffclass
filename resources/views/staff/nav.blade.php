               
    <!-- Nav tabs -->
    <div class="animated fadeIn">
      <div id="btn-nav" class="btn-group btn-group-justified" role="group">
        <div class="btn-group" role="group">
            <a class="btn on btn-default btn-l no-bod-rad" type="button" href="course">
            <i class="fa fa-folder fa-lg"></i><span class="hidden-xs"> &nbsp; Module Outline</span></a>
        </div>
       <!--  <div class="btn-group" role="group">
            <a class="btn btn-default btn-l no-bod-rad" type="button" href="#profile" aria-controls="profile" data-toggle="tab">
            <i class="fa fa-edit fa-lg"></i><span class="hidden-xs"> &nbsp; Scores</span></a>
        </div> -->
        <div class="btn-group"role="group" class="active">
            <a class="btn btn-default btn-l no-bod-rad" type="button" href="course-discussion">
            <i class="fa fa-commenting fa-lg"></i><span class="hidden-xs"> &nbsp; Discussions</span></a>
        </div>
        <div class="btn-group" role="group">
            <a class="btn btn-default btn-l no-bod-rad" type="button" href="course-materials">
            <i class="fa fa-file-archive-o fa-lg"></i><span class="hidden-xs"> &nbsp; Module Materials</span></a>
        </div>
      </div>
    </div>