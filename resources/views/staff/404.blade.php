@extends('layout.main')

@section('content')


<!-- Page Content -->

    

<section id="error-page">
	<div class="container-fluid">
		<div class="row">
		<div class="separator separator-sm"></div>
			<div class="col-sm-12 text-center">
				<span class="circle"><h1 class="h1 animated flash">404</h1></span>
			</div>
			
				<div class="col-sm-offset-2 col-sm-8 text-center">
					<h2>Oops!</h2>
					<p>It looks like you may have taken a wrong turn,
					<br> Don't worry it happens to the best of us</p>
					<br>
					<a class="btn text-capitalize" href="javascript: window.history.back()">headeth backeth to wh're thee cameth from</a>
					<div class="separator separator-sm"></div>
				</div>
		
		</div>
	</div>
</section>

    


@stop


@section('script')

<script>
    $(document).ready(function() {

      $("#owl-demo").owlCarousel({
        items : 4,
        navigation : false,
        navigationText : ["<",">"]
      });

    //  $('.link').on('click', function(event){
    //    var $this = $(this);
    //    if($this.hasClass('clicked')){
    //      $this.removeAttr('style').removeClass('clicked');
    //    } else{
    //      $this.css('background','#7fc242').addClass('clicked');
    //    }
    //  });

    });
</script>
    
@endsection
<!-- /.container --> 