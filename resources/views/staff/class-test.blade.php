@extends('layout.main')

@section('content')

<div class="bg-medium-grey">

<div class="container">

        <br>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
            <div class="paper">
                <h5 class="text-center text-brandon text-uppercase space-lg">Week 1 - <span class="vla-orange-text">CV crafting for Job seekers</span></h5><hr class="hr-sm">
                
                    <h2 class=" text-center">
                        <span class="vla-orange-text">1.3</span> 
                        Continuous Assessment
                    </h2>
                     <!-- <span class="label label-success">Done</span> -->
                    
                
                    <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <p>
                            <b>Instructions comes here</b>
                        </p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam maxime incidunt modi temporibus nobis ut optio sint numquam, reprehenderit dolores ipsam quod aliquam, vitae aliquid minus fugit harum tempora perferendis!</p>
                    </div>
                    
                   <ol class="question-group">
                       <li class="question">
                            <div class="q-text">
                                <p>Introduce the visitor to the business using clear, informative text. Use well-targeted keywords within your sentences to make sure search engines can find the business.</p>                            
                            </div>
                           <div class="q-option">                          
                
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Option one is this and that&mdash;be sure to include why it's great
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Option two can be something else and selecting it will deselect option one
                              </label>
                            </div>
                
                           </div>
                           <div class="alert alert-info no-border q-answer hidden">
                           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <p>Correct Answer - </p>
                               <p>
                                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias fugiat, quidem aut ipsa sapiente tenetur maxime dolore quos at ipsum unde vero, nemo et vitae eum corrupti, odit porro hic.
                               </p>
                           </div>
                       </li>
                
                       <li class="question">
                            <div class="q-text">
                                <p>Introduce the visitor to the business using clear, informative text. Use well-targeted keywords within your sentences to make sure search engines can find the business.</p>                            
                            </div>
                           <div class="q-option">                          
                
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Option one is this and that&mdash;be sure to include why it's great
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Option two can be something else and selecting it will deselect option one
                              </label>
                            </div>
                
                           </div>
                       </li>
                
                       <li class="question">
                            <div class="q-text">
                                <p>Introduce the visitor to the business using clear, informative text. Use well-targeted keywords within your sentences to make sure search engines can find the business.</p>                            
                            </div>
                           <div class="q-option">                          
                
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Option one is this and that&mdash;be sure to include why it's great
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Option two can be something else and selecting it will deselect option one
                              </label>
                            </div>
                
                           </div>
                       </li>
                
                       <li class="question">
                            <div class="q-text">
                                <p>Introduce the visitor to the business using clear, informative text. Use well-targeted keywords within your sentences to make sure search engines can find the business.</p>                            
                            </div>
                           <div class="q-option"> 
                
                           <div class="checkbox">
                              <label>
                                <input type="checkbox" value="">
                                Option one is this and that&mdash;be sure to include why it's great
                              </label>
                            </div>
                            <div class="checkbox ">
                              <label>
                                <input type="checkbox" value="" >
                                Option two is disabled
                              </label>
                            </div>
                
                           </div>
                       </li>
                
                   </ol>

                   <a href="class-result.php" class="btn btn-danger"><i class="fa fa-check"></i>  Submit Test</a>
                   <a href="" class="btn btn-default pull-right"><i class="fa fa-times"></i>  Cancel Test</a>
                   <hr>

                    <!-- <small class="text-muted">(Video and Text: © The Open University (Assets: BBC/Wikimedia commons(made available under the Creative Commons CC0 1.0 Universal Public Domain Dedication - https://creativecommons.org/licenses/by/1.0/)/European Green Capital/European Network of Living Labs, ENoLL/Future Cities Catapult (made available under Creative Commons Attribution-Share Alike Licence)OpenLivingLab Days))</small> -->
                    
                
                    <div class="btn-group btn-group-justified hidden" role="group" aria-label="...">
                      <div class="btn-group" role="group">
                        <a href="class-video.php" type="button" class="no-border btn btn-lg btn-default"><i class="fa fa-chevron-left"></i> &nbsp; PREV <span class="hidden-xs">CLASS</span></a>
                      </div>
                      <div class="btn-group" role="group">
                        <a type="button" class="no-border btn btn-lg btn-default">
                            <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-muted"></i>
                              <i class="fa fa-check fa-stack-1x fa-inverse"></i>
                            </span><br>
                        </a>
                      </div>
                      <div class="btn-group" role="group">
                        <a href="class-test.php" type="button" class="no-border btn btn-lg btn-default"> TAKE TEST &nbsp; <i class="fa fa-chevron-right"></i></a>
                      </div>
                    </div>
                 </div>
                        </div></div>
        <!-- /.row -->

        <!-- Footer -->
        <div class="separator separator-sm"><br></div>

    </div>
</div>

@stop


@section('script')

<script>
    $(document).ready(function() {

      $("#owl-demo").owlCarousel({
        items : 4,
        navigation : true,
        navigationText : ["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"],
        pagination: false
      });

    //  $('.link').on('click', function(event){
    //    var $this = $(this);
    //    if($this.hasClass('clicked')){
    //      $this.removeAttr('style').removeClass('clicked');
    //    } else{
    //      $this.css('background','#7fc242').addClass('clicked');
    //    }
    //  });

    });
</script>

@endsection
<!-- /.container --> 