@extends('layout.main') @section('content')


<!-- Page Content -->

<header class="course-header dash no-bg-img" style="">
	<div class="container-fluid">
		<div class="col-md-10 col-md-offset-1 row">
			<div class="col-sm-4">
				<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="img-top-core" width="100%">
			</div>
			<div class="col-sm-8">
				<h1 class=" text-shadow-xs tagline text-bold no-margin">Title of Course comes here</h1>
				<p class=" text-shadow-xs ">Description of Course comes here. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, laboriosam, laudantium maiores harum ab minus. Magnam ducimus quis fugiat debitis unde non, laborum quae, eos voluptas laudantium id dolor doloribus!</p>
				<!-- <div class="separator-sm"></div> -->
				<!-- <div class="progress">
                    <div class="one danger-color"></div><div class="two no-color"></div><div class="three no-color"></div>
                        <div class="progress-bar progress-bar-danger" style="width: 30%"></div>
                    </div> -->
				<br>
				<a href="course2" class="btn btn-lg btn-danger vla-red-bg text-brandon text-uppercase">  &nbsp; 25% Done &nbsp; | &nbsp; Continue <span class="hidden-xs">Course</span>  &nbsp; <i class="fa fa-chevron-right"></i></a>

			</div>
			<div class="clearfix hidden-xs">
				<br>
			</div>
			<br>

			<!--
			<div class="col-sm-6">
             
			</div>

			<div class="col-sm-6">
				<div class="well no-border text-white text-center" style="background:#45474d">
					<p class="lead">
						<i class="fa fa-star-o"></i>
						<i class="fa fa-star-o"></i>
						<i class="fa fa-star-o"></i>
					</p>
					<p>You have unlocked 3 badges, unlock more badges- take more courses and stay ahead on the leaderboard.</p>

					<a href="">
						<img style="border: 5px solid #fff;border-radius: 50%;margin: 2%;" width="25%" src="https://www.codeschool.com/assets/paths/badge-javascript-min-61c3e5858a6e6aee7aa5715f8d8824deaf087b6fa689df71451ea864805832f7.svg" alt="">
					</a>
					<a href="">
						<img style="border: 5px solid #fff;border-radius: 50%;margin: 2%;" width="25%" src="https://www.codeschool.com/assets/paths/badge-ruby-min-d25a54bbab64a6e8b4fe1558748a49ee8c71887d0b65630a0cf0dab4791c57dc.svg" alt="">
					</a>
					<a href="">
						<img style="border: 5px solid #fff;border-radius: 50%;margin: 2%;" width="25%" src="https://www.codeschool.com/assets/paths/badge-electives-min-04558aa07be6a8e932309a7575c158c1922d5ed80bb27d510a43087951d0855a.svg" alt="">
					</a>
					<br>
					<br>
					<p>
						<a href="#leadboard" class="btn btn-lg btn-default text-uppercase"><i class="fa fa-trophy"></i> &nbsp;see leaderboard</a>
					</p>
				</div>
			</div>
-->


		</div>
	</div>
</header>
<!--Header end-->



<section class="center-text library-body">
	<div class="row">
		<ul class="nav nav-tabs nav-justified" role="tablist">
			<li role="presentation" class="active">
				<a href="#ongoingCourses" aria-controls="ongoingCourses" role="tab" data-toggle="tab" class="library-body">
					<i class="fa fa-th fa-lg"></i><span class="hidden-xs"> &nbsp; Ongoing Courses</span></a>
			</li>

			<li role="presentation">
				<a href="#completedCourses" aria-controls="completedCourses" role="tab" data-toggle="tab" class="library-body">
					<i class="fa fa-list fa-lg"></i><span class="hidden-xs"> &nbsp; Completed Courses</span></a>
			</li>
		</ul>
	</div>
	<div class="container-fluid">
		<div class="col-md-10 col-md-offset-1">
			<div class="tab-content tab-home">
				<div role="tabpanel" class="tab-pane active" id="ongoingCourses">
					<div class="panel-group">
						<div class="row">
							<div class="col-md-4 col-sm-6  animated zoomIn">
								<div class="panel panel-default course-item">
									<a href="course2" class="text-center course-link">
										<img src="{{asset('assets/img/week1.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
										<h4>Successful Negotiation</h4>
										<hr class="text-center course-underline">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
										<br>
										<span class="text-center col-xs-12">
                               <button class="btn btn-success ">View Course</button>	
                                </span>
										<br>
									</a>
								</div>
							</div>

							<div class="col-md-4 col-sm-6 animated zoomIn">
								<div class="panel panel-default course-item">
									<a href="" class="text-center course-link">
										<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
										<h4>Positive Psychology</h4>
										<hr class="text-center course-underline">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
										<br>
										<span class="text-center col-xs-12">
                <button class="btn btn-success ">View Course</button>	
               </span>
										<br>
									</a>
								</div>
							</div>


							<div class="col-md-4 col-sm-6 animated zoomIn">
								<div class="panel panel-default course-item">
									<a href="course2" class="text-center course-link">
										<img src="{{asset('assets/img/week3.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
										<h4>Business Foundations</h4>
										<hr class="text-center course-underline">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
										<br>
										<span class="text-center col-xs-12">
								<button class="btn btn-success ">View Course</button>	
							</span>
										<br>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div role="tabpanel" class="tab-pane" id="completedCourses">
					<div class="panel-group">
						<div class="row">
							<div class="col-md-4 col-sm-6  animated zoomIn">
								<div class="panel panel-default course-item">
									<a href="course2" class="text-center course-link">
										<img src="{{asset('assets/img/week1.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
										<h4>Negotiation</h4>
										<hr class="text-center course-underline">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
										<br>
										<span class="text-center col-xs-12">
                               <button class="btn btn-success ">View Course</button>	
                                </span>
										<br>
									</a>
								</div>
							</div>

							<div class="col-md-4 col-sm-6 animated zoomIn">
								<div class="panel panel-default course-item">
									<a href="" class="text-center course-link">
										<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
										<h4>Positive</h4>
										<hr class="text-center course-underline">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
										<br>
										<span class="text-center col-xs-12">
                <button class="btn btn-success ">View Course</button>	
               </span>
										<br>
									</a>
								</div>
							</div>


							<div class="col-md-4 col-sm-6 animated zoomIn">
								<div class="panel panel-default course-item">
									<a href="course2" class="text-center course-link">
										<img src="{{asset('assets/img/week3.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
										<h4>Business</h4>
										<hr class="text-center course-underline">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
										<br>
										<span class="text-center col-xs-12">
								<button class="btn btn-success ">View Course</button>	
							</span>
										<br>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<!--		<a href="library" class="text-center btn btn-default">See all available courses</a>-->
			<div class="separator separator-sm"></div>
		</div>
	</div>
</section>


@stop @section('script') @endsection
<!-- /.container -->