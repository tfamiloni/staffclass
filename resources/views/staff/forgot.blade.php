@extends('layout.auth') @section('content')


<!-- Page Content -->
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">



			<form class="well">
				<div class="heading text-center">
					<h4 class="text-brandon text-uppercase space-lg text-center text-white"><img src="{{asset('assets/img/logomark.png')}}" alt="" height="35px"></h4>
					<span class="text-uppercase vla-red-text space-lg small">academy</span>
				</div>

				<div class="col-md-12 text-center">
					<h3 class="text-uppercase">You forgot your password?</h3>
					<p>No worries, we’ve got your back :)
						<br>Just type in the email you opened this account with and we will send you a link back in!
						<br>
					</p>
				</div>
				<div class="col-md-12">
					<div class="controls">
						<input class="input-lg form-control text-center" id="inputUser" placeholder="Enter Staff ID here" type="text">
					</div>
					<br>
				</div>
				<div class="col-md-12">
					<div class="controls text-center">
						<br>
						<a href="" class="btn btn-success btn-block btn-lg text-uppercase small" type="submit">Retrieve&nbsp; <i class="small fa fa-arrow-right"></i></a>
						<!-- <button class="btn" type= "button">Help</button> -->
					</div>
					<br><br>
				</div>

			</form>

			<div class="">
				<small class="center-block text-center text-white">&copy; 2016. <a href="_index.php" class="text-white">Vic Lawerence & Associates</a></small>
				<br>
			</div>
		</div>
	</div>
</div>

@stop
<!-- /.container -->