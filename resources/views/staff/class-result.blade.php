@extends('layout.main')

@section('content')


<div class="bg-medium-grey">
	<div class="container">

        <br>

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
            <div class="paper">
                <h5 class="text-center text-brandon text-uppercase space-lg">Week 1 - <span class="vla-orange-text">CV crafting for Job seekers</span></h5><hr class="hr-sm">
                
                    <h2 class=" text-center">
                        <span class="vla-orange-text">1.3</span> 
                        Continuous Assessment
                    </h2>
                     <!-- <span class="label label-success">Done</span> -->
                    
                    <div class="alert alert-success center-block">
                        <a href="class-test.php " class="btn btn-default pull-right">Retake Test</a>
                        <h2 class="pull-left no-margin">Score: 3/5</h2>
                        <div class="clearfix"></div>
                    </div><br>
                    
                    <p class="text-center">Kindly review answer to questionmarked in red</p>
                   <ol class="question-group">
                       <li class="question wrong">
                            <div class="q-text">
                                <p>Introduce the visitor to the business using clear, informative text. Use well-targeted keywords within your sentences to make sure search engines can find the business.</p>                            
                            </div>
                           <div class="q-option">                          
                
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Option one is this and that&mdash;be sure to include why it's great
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Option two can be something else and selecting it will deselect option one
                              </label>
                            </div>
                
                           </div>
                           <div class="alert alert-info no-border q-answer">
                           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <p>Correct Answer - </p>
                               <p>
                                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias fugiat, quidem aut ipsa sapiente tenetur maxime dolore quos at ipsum unde vero, nemo et vitae eum corrupti, odit porro hic.
                               </p>
                           </div>
                       </li>
                       <li class="question wrong">
                            <div class="q-text">
                                <p>Introduce the visitor to the business using clear, informative text. Use well-targeted keywords within your sentences to make sure search engines can find the business.</p>                            
                            </div>
                           <div class="q-option">                          
                
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
                                Option one is this and that&mdash;be sure to include why it's great
                              </label>
                            </div>
                            <div class="radio">
                              <label>
                                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                                Option two can be something else and selecting it will deselect option one
                              </label>
                            </div>
                
                           </div>
                           <div class="alert alert-info no-border q-answer">
                           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <p>Correct Answer - </p>
                               <p>
                                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias fugiat, quidem aut ipsa sapiente tenetur maxime dolore quos at ipsum unde vero, nemo et vitae eum corrupti, odit porro hic.
                               </p>
                           </div>
                       </li>
                
                   </ol>

                   <p>&nbsp; You can only retake a test once</p>
                   <a href="class-test.php" class="btn btn-default"><i class="fa fa-edit"></i>  Retake Test</a>
                   <a href="week-3.php" class="btn btn-success pull-right"><i class="fa fa-edit"></i> Proceed &raquo; </a>
                   
                    <hr>
                    <!-- <small class="text-muted">(Video and Text: © The Open University (Assets: BBC/Wikimedia commons(made available under the Creative Commons CC0 1.0 Universal Public Domain Dedication - https://creativecommons.org/licenses/by/1.0/)/European Green Capital/European Network of Living Labs, ENoLL/Future Cities Catapult (made available under Creative Commons Attribution-Share Alike Licence)OpenLivingLab Days))</small> -->
                    
                 </div>
                </div></div>
        <!-- /.row -->

        <!-- Footer -->
        <div class="separator separator-sm"><br></div>

    </div>
</div>



@stop


@section('script')

<script>
    $(document).ready(function() {

      $("#owl-demo").owlCarousel({
        items : 4,
        navigation : true,
        navigationText : ["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"],
        pagination: false
      });

    //  $('.link').on('click', function(event){
    //    var $this = $(this);
    //    if($this.hasClass('clicked')){
    //      $this.removeAttr('style').removeClass('clicked');
    //    } else{
    //      $this.css('background','#7fc242').addClass('clicked');
    //    }
    //  });

    });
</script>

@endsection
<!-- /.container --> 