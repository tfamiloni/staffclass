@extends('layout.main')

@section('content')


<!-- Page Content -->
@include ('staff.includes.progress-section')               
    <!-- Nav tabs -->
    <div class="animated fadeIn">
      <div class="btn-group btn-group-justified" role="group">
        <div class="btn-group" role="group">
            <a class="btn btn-default btn-l no-bod-rad" type="button" href="course2">
            <i class="fa fa-folder fa-lg"></i><span class="hidden-xs"> &nbsp; Module Outline</span></a>
        </div>
       <!--  <div class="btn-group" role="group">
            <a class="btn btn-default btn-l no-bod-rad" type="button" href="#profile" aria-controls="profile" data-toggle="tab">
            <i class="fa fa-edit fa-lg"></i><span class="hidden-xs"> &nbsp; Scores</span></a>
        </div> -->
        <div class="btn-group"role="group" class="active">
            <a class="btn btn-default btn-l no-bod-rad on" type="button" href="course-discussion">
            <i class="fa fa-commenting fa-lg"></i><span class="hidden-xs"> &nbsp; Discussions</span></a>
        </div>
        <div class="btn-group" role="group">
            <a class="btn btn-default btn-l no-bod-rad" type="button" href="course-materials">
            <i class="fa fa-file-archive-o fa-lg"></i><span class="hidden-xs"> &nbsp; Module Materials</span></a>
        </div>
      </div>
    </div>
    
    
    <div class="container">

        <br>

        <div class="row">
            
        <div class="col-sm-12 col-md-8 col-md-offset-2">
               
               <div id="comment-section">
         <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="home">
                  <h3 class=" text-center">Module 1 Course Discussions</h3>

                <div id="" class="comment">
<!--                    <h5 class="text-brandon text-uppercase text-green-deep text-center">7 comments so far</h5>-->
                    <hr>
                    
                    <div class="media hidden"> 
                    
                        <div class="media-left"> 
                            <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/300x300/ffffff/17ba2a&amp;text=AB"> 
                            </a> 
                        </div> 
                    
                        <div class="media-body"> 
                            <h4 class="media-heading">Media heading
                            </h4> 
                    
                            <span><span class="comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</span> </span>
                            
                            <div class="reply-sec">
                                <p style="margin-top:7px">
                                    <a class="small" role="button" data-toggle="collapse" href="#replyForm" aria-expanded="false" aria-controls="replyForm"><i class="fa fa-commenting"></i> Reply to comment</a>
                                </p>
                                <div class="collapse" id="replyForm">
                                    <form action="">
                                        <textarea name="" id="" class="form-control form-group"></textarea>
                                        <input type="submit" class="btn btn-sm" value="Comment"><hr>
                                    </form>
                                </div>
                            </div>
                    
                            <div class="media"> 
                                <div class="media-left"> 
                                    <a href="#"> 
                                        <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM2ZmQ2NyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzZmZDY3Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;"> 
                                        </a> 
                                    </div> 
                                <div class="media-body">
                                    <h4 class="media-heading">Nested media heading</h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus. 
                                </div> <br>
                            </div>
                    
                            <div class="media"> 
                                <div class="media-left"> 
                                    <a href="#"> 
                                        <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM2ZmQ2NyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzZmZDY3Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;"> 
                                        </a> 
                                    </div> 
                                <div class="media-body">
                                    <h4 class="media-heading">Nested media heading</h4> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus. 
                    
                    
                                </div> <br>
                            </div>
                            
                        </div> 
                        <br>
                    </div>
                    

                    

                   
                    <div class="media" id="comment-section"> 
                        <div class="media-left"> 
                        <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/000000&amp;text=OA"> 
                        <!-- <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">  -->
                        </a> 
                        </div> 
                        <div class="media-body"> 
                            <h4 class="media-heading">Oshunlana Ayodele <small>made a comment</small> <br>
                            <small>LESSON 10: HOW TO IMPRESS FOR SUCCESS WITH EMAIL AND PHONE-CALL FOLLOW UP</small>
                            </h4> I like this 
                            
                                    <div class="reply-sec">
                                        <p style="margin-top:7px">
                                            <a class="small" role="button" data-toggle="collapse" href="#replyForm1" aria-expanded="false" aria-controls="replyForm2"><i class="fa fa-commenting"></i> Reply to comment</a>
                                        </p>
                                        <div class="collapse" id="replyForm1">
                                            <form id="ReplyComment1" action="http://learnn.insidify.com/ajax-comment" method="post">
                                                 <input type="hidden" name="_token" value="LhRblEibBIwD0vnXvQWGliEcboBf0AShqeYDfMd0">
                                                
                                                <input type="hidden" name="comment_target" value="COMMENT">
                                                <input type="hidden" name="comment_target_id" value="1">

                                                <textarea name="comment" id="" class="form-control form-group" required=""></textarea>
                                                <input type="submit" id="SubmitReply1" class="btn btn-sm" value="Comment"><hr>
                                            </form>
                                        </div>
                                    </div>
                                                                        
                                                                             <div class="media"> 
                                            <div class="media-left"> 
                                                <a href="#"> 
                                                     <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/000000&amp;text=OA"> 
                                                    </a> 
                                                </div> 
                                            <div class="media-body">
                                                <h4 class="media-heading">Oshunlana Ayodele</h4> What do you like about it?
                                            </div> <br>
                                        </div>
                                                                                                 </div> 
                    </div>
      
                    <div class="media" id="comment-section"> 
                        <div class="media-left"> 
                        <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/17ba2a&amp;text=OA"> 
                        <!-- <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">  -->
                        </a> 
                        </div> 
                        <div class="media-body"> 
                            <h4 class="media-heading">Oshunlana Ayodele <small>made a comment</small> <br>
                            <small>LESSON 2  –  HOW TO PASS APTITUDE TESTS: LEVEL 1</small>
                            </h4> Lovely class. Looking forward to the next. 
                            
                                    <div class="reply-sec">
                                        <p style="margin-top:7px">
                                            <a class="small" role="button" data-toggle="collapse" href="#replyForm3" aria-expanded="false" aria-controls="replyForm2"><i class="fa fa-commenting"></i> Reply to comment</a>
                                        </p>
                                        <div class="collapse" id="replyForm3">
                                            <form id="ReplyComment3" action="http://learnn.insidify.com/ajax-comment" method="post">
                                                 <input type="hidden" name="_token" value="LhRblEibBIwD0vnXvQWGliEcboBf0AShqeYDfMd0">
                                                
                                                <input type="hidden" name="comment_target" value="COMMENT">
                                                <input type="hidden" name="comment_target_id" value="3">

                                                <textarea name="comment" id="" class="form-control form-group" required=""></textarea>
                                                <input type="submit" id="SubmitReply3" class="btn btn-sm" value="Comment"><hr>
                                            </form>
                                        </div>
                                    </div>
                                                                                                                                   </div> 
                    </div>

                   
                    <div class="media" id="comment-section"> 
                        <div class="media-left"> 
                        <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/17ba2a&amp;text=OA"> 
                        <!-- <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">  -->
                        </a> 
                        </div> 
                        <div class="media-body"> 
                            <h4 class="media-heading">Oshunlana Ayodele <small>made a comment</small> <br>
                            <small>LESSON 1: INTRODUCTION TO THE MODULE &amp; UNDERSTANDING JOB SEARCH IN NIGERIA</small>
                            </h4> I like this. Thanks 
                            
                                    <div class="reply-sec">
                                        <p style="margin-top:7px">
                                            <a class="small" role="button" data-toggle="collapse" href="#replyForm4" aria-expanded="false" aria-controls="replyForm2"><i class="fa fa-commenting"></i> Reply to comment</a>
                                        </p>
                                        <div class="collapse" id="replyForm4">
                                            <form id="ReplyComment4" action="http://learnn.insidify.com/ajax-comment" method="post">
                                                 <input type="hidden" name="_token" value="LhRblEibBIwD0vnXvQWGliEcboBf0AShqeYDfMd0">
                                                
                                                <input type="hidden" name="comment_target" value="COMMENT">
                                                <input type="hidden" name="comment_target_id" value="4">

                                                <textarea name="comment" id="" class="form-control form-group" required=""></textarea>
                                                <input type="submit" id="SubmitReply4" class="btn btn-sm" value="Comment"><hr>
                                            </form>
                                        </div>
                                    </div>
                                                                                                                                   </div> 
                    </div>


                     

                   
                    <div class="media" id="comment-section"> 
                        <div class="media-left"> 
                        <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/17ba2a&amp;text=OA"> 
                        <!-- <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">  -->
                        </a> 
                        </div> 
                        <div class="media-body"> 
                            <h4 class="media-heading">Oshunlana Ayodele <small>made a comment</small> <br>
                            <small>LESSON 2: JOB SEARCH IS VALUE EXCHANGE</small>
                            </h4> Thanks 
                            
                                    <div class="reply-sec">
                                        <p style="margin-top:7px">
                                            <a class="small" role="button" data-toggle="collapse" href="#replyForm6" aria-expanded="false" aria-controls="replyForm2"><i class="fa fa-commenting"></i> Reply to comment</a>
                                        </p>
                                        <div class="collapse" id="replyForm6">
                                            <form id="ReplyComment6" action="http://learnn.insidify.com/ajax-comment" method="post">
                                                 <input type="hidden" name="_token" value="LhRblEibBIwD0vnXvQWGliEcboBf0AShqeYDfMd0">
                                                
                                                <input type="hidden" name="comment_target" value="COMMENT">
                                                <input type="hidden" name="comment_target_id" value="6">

                                                <textarea name="comment" id="" class="form-control form-group" required=""></textarea>
                                                <input type="submit" id="SubmitReply6" class="btn btn-sm" value="Comment"><hr>
                                            </form>
                                        </div>
                                    </div>
                                                                        
                                                                             <div class="media"> 
                                            <div class="media-left"> 
                                                <a href="#"> 
                                                     <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/17ba2a&amp;text=HO"> 
                                                    </a> 
                                                </div> 
                                            <div class="media-body">
                                                <h4 class="media-heading">Hawks Ololade </h4> Hmmmmm. Good stuff
                                            </div> <br>
                                        </div>
                                                                                                 </div> 
                    </div>


                   
                    <div class="media" id="comment-section"> 
                        <div class="media-left"> 
                        <a href="#"> 
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="http://dummyimage.com/64x64/f5f2f5/000000&amp;text=OA"> 
                        <!-- <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">  -->
                        </a> 
                        </div> 
                        <div class="media-body"> 
                            <h4 class="media-heading">Oshunlana Ayodele <small>made a comment</small> <br>
                            <small>LESSON 2: UNDERSTANDING THE ROLE &amp; IMPORTANCE OF A REMARKABLE RESUME</small>
                            </h4> Thank you. I love this course 
                            
                                    <div class="reply-sec">
                                        <p style="margin-top:7px">
                                            <a class="small" role="button" data-toggle="collapse" href="#replyForm8" aria-expanded="false" aria-controls="replyForm2"><i class="fa fa-commenting"></i> Reply to comment</a>
                                        </p>
                                        <div class="collapse" id="replyForm8">
                                            <form id="ReplyComment8" action="http://learnn.insidify.com/ajax-comment" method="post">
                                                 <input type="hidden" name="_token" value="LhRblEibBIwD0vnXvQWGliEcboBf0AShqeYDfMd0">
                                                
                                                <input type="hidden" name="comment_target" value="COMMENT">
                                                <input type="hidden" name="comment_target_id" value="8">

                                                <textarea name="comment" id="" class="form-control form-group" required=""></textarea>
                                                <input type="submit" id="SubmitReply8" class="btn btn-sm" value="Comment"><hr>
                                            </form>
                                        </div>
                                    </div>
                                                                                                                                   </div> 
                    </div>

                    <div class="separator separator-xs"></div>
                                   
              </div>
              <!-- Tab1 -->



            </div>

          </div>

        </div>
                
       </div>
        </div>
            

        <!-- /.row -->


            

                <div class="row">
            </div>
        <!-- Footer -->

    </div>
    
<!--
<section class="center-text">
    <div class="container">
        <div class="row">
            More courses section
        </div>
    </div>
</section>
-->




@stop


@section('script')

<script>
    $(document).ready(function() {

      $("#owl-demo").owlCarousel({
        items : 4
      });

    //  $('.link').on('click', function(event){
    //    var $this = $(this);
    //    if($this.hasClass('clicked')){
    //      $this.removeAttr('style').removeClass('clicked');
    //    } else{
    //      $this.css('background','#7fc242').addClass('clicked');
    //    }
    //  });

    });
</script>
   
<script>
    function scrollWin() {
        window.scrollTo(0, 300);
    }
    
    scrollWin()
</script>
    
@endsection
<!-- /.container --> 