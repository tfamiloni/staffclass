@extends('layout.main') @section('content')

<!-- Page Content -->

<header class="course-header dash no-bg-img">
	<div class="container">
		<div class="row">

			<h1 class="tagline text-bold text-shadow-xs text-center">VLA Library</h1>
			
			<p class="lead no-margin text-center">
				<span class="fa fa-university"></span> 15 Categories
             
			     &nbsp; &middot; &nbsp; 
               
                <span class="fa fa-list"></span> 200 Courses
                
                 &nbsp; &middot; &nbsp; 
                 
                <span class="fa fa-user"></span> 21 Speakers
			</p>
			
		</div>


	</div>
</header>

<!-- Page Content -->
<section class="library-body">
<div class="container">

	<div class="row">
		<div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
			<div>

				<!-- Nav tabs -->
				<div class="row">
<!--
					<div class="btn-group btn-group-justified" role="group">
						<div class="btn-group" role="group" class="active">
							<a class="btn btn-default on btn-l no-bod-rad library-body" type="button" href="#library" aria-controls="home" data-toggle="tab">
								<i class="fa fa-th fa-lg"></i><span class="hidden-xs"> &nbsp; All Courses</span></a>
						</div>

						<div class="btn-group" role="group">
							<a class="btn btn-default btn-l no-bod-rad library-body" type="button" href="categories">
								<i class="fa fa-list fa-lg"></i><span class="hidden-xs"> &nbsp; Categories</span></a>
						</div>

					</div>
-->
					<ul class="nav nav-tabs nav-justified" role="tablist">
						<li role="presentation" class="active">
							<a href="#allCourses" aria-controls="allCourses" role="tab" data-toggle="tab" class="library-body"><i class="fa fa-th fa-lg"></i><span class="hidden-xs"> &nbsp; All Courses</span></a>
						</li>
						<li role="presentation">
							<a href="#categories" aria-controls="categories" role="tab" data-toggle="tab" class="library-body"><i class="fa fa-list fa-lg"></i><span class="hidden-xs"> &nbsp; Categories</span></a>
						</li>
					</ul>
				</div>

				<div class="tab-content tab-home">
					<div role="tabpanel" class="tab-pane active" id="allCourses">
						<div class="panel-group">

							<div class="row">
								<div class="col-md-4 col-sm-6 animated zoomIn">
									<div class="panel panel-default course-item">
										<a href="course2" class="text-center course-link">
											<img src="{{asset('assets/img/week1.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
											<h4>Successful Negotiation</h4>
											<hr class="text-center course-underline">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
											<br>
											<span class="text-center col-xs-12">
												<button class="btn btn-success">View Course</button>	
											</span>
											<br>
											<br>
										</a>
									</div>
								</div>
								
								<div class="col-md-4 col-sm-6 animated zoomIn">
									<div class="panel panel-default course-item">
										<a href="course2" class="text-center course-link">
											<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
											<h4>Positive Psychology</h4>
											<hr class="text-center course-underline">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
											<br>
											<span class="text-center col-xs-12">
											<button class="btn btn-success ">View Course</button>	
											</span>
											<br><br>
										</a>
									</div>
								</div>
								
								<div class="col-md-4 col-sm-6 animated zoomIn">
									<div class="panel panel-default course-item">
										<a href="course2" class="text-center course-link">
											<img src="{{asset('assets/img/week3.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
											<h4>Business Foundations</h4>
											<hr class="text-center course-underline">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
											<br>
											<span class="text-center col-xs-12">
											<button class="btn btn-success ">View Course</button>	
											</span>
											<br><br>
										</a>
									</div>
								</div>
								
								
									<div class="col-md-4 col-sm-6 animated zoomIn">
									<div class="panel panel-default course-item">
										<a href="course2" class="text-center course-link">
											<img src="{{asset('assets/img/week1.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
											<h4>Successful Negotiation</h4>
											<hr class="text-center course-underline">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
											<br>
											<span class="text-center col-xs-12">
											<button class="btn btn-success ">View Course</button>	
											</span>
											<br><br>
										</a>
									</div>
								</div>
								
								<div class="col-md-4 col-sm-6 animated zoomIn">
									<div class="panel panel-default course-item">
										<a href="course2" class="text-center course-link">
											<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
											<h4>Positive Psychology</h4>
											<hr class="text-center course-underline">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
											<br>
											<span class="text-center col-xs-12">
											<button class="btn btn-success ">View Course</button>	
											</span>
											<br><br>
										</a>
									</div>
								</div>
								
								<div class="col-md-4 col-sm-6 animated zoomIn">
									<div class="panel panel-default course-item">
										<a href="course2" class="text-center course-link">
											<img src="{{asset('assets/img/week3.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
											<h4>Business Foundations</h4>
											<hr class="text-center course-underline">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
											<br>
											<span class="text-center col-xs-12">
											<button class="btn btn-success ">View Course</button>	
											</span>
											<br><br>
										</a>
									</div>
								</div>
								
								

							</div>

						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="categories">
						<div class="panel-group">

							<a href="#">
								<div class="panel panel-default panel-course category-item animated slideInLeft">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week1.jpg')}}" alt="" class="category-icon" height="90px" width="auto">	
										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Personal Development <small> &nbsp; 12 courses</small> </h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>
							
							<a href="#">
								<div class="panel panel-default panel-course category-item animated slideInRight">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="category-icon" height="90px" width="auto">	
										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Language Learning <small>&nbsp; 6 courses</small></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>
							
							<a href="#">
								<div class="panel panel-default panel-course category-item animated slideInLeft">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week3.jpg')}}" alt="" class="category-icon" height="90px" width="auto">

										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Business and Social Sciences <small>&nbsp; 25 courses</small></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>

							
							<a href="#">
								<div class="panel panel-default panel-course category-item">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week1.jpg')}}" alt="" class="category-icon" height="90px" width="auto">	
										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Personal Development <small> &nbsp; 12 courses</small> </h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>
							
							<a href="#">
								<div class="panel panel-default panel-course category-item">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week2.jpg')}}" alt="" class="category-icon" height="90px" width="auto">	
										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Language Learning <small>&nbsp; 6 courses</small></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>
							
							<a href="#">
								<div class="panel panel-default panel-course category-item">
									<div class="row category-row">
										<div class="col-md-3 col-sm-3 hidden-xs">
											<img src="{{asset('assets/img/week3.jpg')}}" alt="" class="category-icon" height="90px" width="auto">	
										</div>
										<div class="col-md-7 col-sm-9 col-xs-12">
											<h4>Business and Social Sciences <small>&nbsp; 25 courses</small></h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, esse nam. Deserunt delectus ex enim assumenda.</p>
										</div>
										<div class="col-md-2 hidden-sm hidden-xs category-item">
											<button class="btn btn-success pull-right">View Category</button>
										</div>
									</div>
								</div>
							</a>


						</div>
					</div>
				</div>


			</div>

		</div>
	</div>
	<!-- /.row -->





	<!-- Footer -->

</div>
</section>


@stop @section('script')

<script>
	$(document).ready(function () {

		$("#owl-demo").owlCarousel({
			items: 4
		});

		//  $('.link').on('click', function(event){
		//    var $this = $(this);
		//    if($this.hasClass('clicked')){
		//      $this.removeAttr('style').removeClass('clicked');
		//    } else{
		//      $this.css('background','#7fc242').addClass('clicked');
		//    }
		//  });

	});
</script>

@endsection
<!-- /.container -->