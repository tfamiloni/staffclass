
<header class="course-header dash" style="">
     <div class="container-fluid">
        <div class="row">
               <div class="">
<!--
                        <div class="col-sm-1 col-xs-2">
                            <img src="{{asset('assets/img/user-male.png')}}" alt="" class="img-circle" width="60px" height="60px">
                        </div>
-->
                        <div class="col-sm-10 col-sm-offset-1 text-center"><br>
                            <h2 class="no-margin text-shadow-xs text-white">The Course Title will appear here</h2>
                            <p class="no-margin">This course has 10 modules, you have completed 1 module(s) &rarr;</span></p><br>
                        </div>
                    </div>
                <div class="col-sm-12">
         <div id="owl-demo" class="owl-carousel modules">
                         
                          
                          <a href="course" class="mod active item link animated bounceIn">
                              <div class="row">
                                  <div class="col-xs-4 text-center animated zoomInRight">
                                      <h1 class="vla-orange-text">25</h1>
                                  </div>
                                  <div class="col-xs-8">
                                      <h5>Module 1: Title of Module comes here as desired</h5>
                                      <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                                  </div>
                              </div>
                          </a>
                          
                          
                          <a href="course" class="mod in-progress item link animated bounceIn">
                              <div class="row">
                                  <div class="col-xs-4 text-center animated zoomInRight">
                                      <h1 class="vla-orange-text">15</h1>
                                  </div>
                                  <div class="col-xs-8">
                                      <h5>Module 2: Title of Module comes here as desired</h5>
                                      <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                                  </div>
                              </div>
                          </a>
                          
                          
                          <a href="course" class="mod in-progress item link animated bounceIn">
                              <div class="row">
                                  <div class="col-xs-4 text-center animated zoomInRight">
                                      <h1 class="vla-orange-text">55</h1>
                                  </div>
                                  <div class="col-xs-8">
                                      <h5>Module 3: Title of Module comes here as desired</h5>
                                      <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                                  </div>
                              </div>
                          </a>
                          
                          <a href="course" class="mod item in-done link animated bounceIn ">
                              <div class="row">
                                  <div class="col-xs-4 text-center animated zoomInRight">
                                      <h1><i class="fa fa-check text-green-deep"></i></h1>
                                  </div>
                                  <div class="col-xs-8">
                                      <h5>Module 4: Title of Module comes here as desired</h5>
                                      <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                                  </div>
                              </div>
                          </a>
                          
                          <a href="course" class="mod finis item link animated bounceIn">
                              <div class="row">
                                  <div class="col-xs-4 text-center animated-slow zoomInRight">
                                      <h1><i class="fa fa-check"></i></h1>
                                  </div>
                                  <div class="col-xs-8">
                                      <h5>Module 5: Title of Module comes here as desired</h5>
                                      <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                                  </div>
                              </div>
                          </a>
                          
                          <a href="course" class="mod finis item link animated bounceIn">
                              <div class="row">
                                  <div class="col-xs-4 text-center animated-slow zoomInRight">
                                      <h1><i class="fa fa-check"></i></h1>
                                  </div>
                                  <div class="col-xs-8">
                                      <h5>Module 6: Title of Module comes here as desired</h5>
                                      <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                                  </div>
                              </div>
                          </a>
                          
                          <a href="course" class="mod finis item link">
                              <div class="row">
                                  <div class="col-xs-4 text-center">
                                      <h1><i class="fa fa-check"></i></h1>
                                  </div>
                                  <div class="col-xs-8">
                                      <h5>TModule 7: Title of Module comes here as desired</h5>
                                      <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                                  </div>
                              </div>
                          </a>
                        </div>
     </div>
    </div>
</div>
</header>
  
 
