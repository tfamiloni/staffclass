@extends('layout.main')

@section('content')


<!-- Page Content -->

    <header class="course-header dash" style="background-image:none;">
        <div class="container-fluid">
            <div class="row">
               <div class="">
                        <div class="col-sm-1 col-xs-2">
                            <img src="{{asset('assets/img/user-image.jpg')}}" alt="" class="img-circle" width="60px" height="60px">
                        </div>
                        <div class="col-sm-8 col-xs-10">
                            <p class="lead"><strong class="">Welcome Olufemi Badejo,</strong><br>
                            <small>You have 3 uncompleted courses & 9 completed courses</small> &rarr;</p>
                        </div>
                        <div class="col-sm-3 hidden-xs">
                            <a href="library" class="btn btn-block btn-lg btn-default text-uppercase text-bold">start a New Course</a>
                        </div>
                    </div>
                    
                <div class="row">
                    

                    <div id="owl-demo" class="owl-carousel">
                     
                      
                      <a href="course" class="in-progress item link animated bounceIn">
                          <div class="row">
                             <div class="img-holder">
                                 <div class="img-holder"><img src="{{asset('assets/img/article1.jpg')}}" alt="" class="" width="120%"></div>
                             </div>
                              <div class="col-xs-4 text-center animated zoomInRight">
                                  <h1 class="vla-orange-text">25</h1>
                              </div>
                              <div class="col-xs-8">
                                  <h4>Title of Course comes and sit right here</h4>
                                  <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                              </div>
                          </div>
                      </a>
                      
                      
                      <a href="course" class="in-progress item link animated bounceIn">
                          <div class="row">
                             <div class="img-holder">
                                 <div class="img-holder"><img src="{{asset('assets/img/week3.jpg')}}" alt="" class="" width="120%"></div>
                             </div>
                              <div class="col-sm-4 text-center animated zoomInRight">
                                  <h1 class="vla-orange-text">15</h1>
                              </div>
                              <div class="col-sm-8">
                                  <h4>Title of Course comes and sit right here</h4>
                                  <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                              </div>
                          </div>
                      </a>
                      
                      
                      <a href="course" class="in-progress item link animated bounceIn">
                          <div class="row">
                             <div class="img-holder">
                                 <div class="img-holder"><img src="{{asset('assets/img/bg-auth.jpg')}}" alt="" class="" width="120%"></div>
                             </div>
                              <div class="col-sm-4 text-center animated zoomInRight">
                                  <h1 class="vla-orange-text">55</h1>
                              </div>
                              <div class="col-sm-8">
                                  <h4>Title of Course comes and sit right here</h4>
                                  <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                              </div>
                          </div>
                      </a>
                      
                      <a href="course" class="item in-done link animated bounceIn ">
                          <div class="row">
                             <div class="img-holder">
                                 <div class="img-holder"><img src="{{asset('assets/img/article2.jpg')}}" alt="" class="" width="120%"></div>
                             </div>
                              <div class="col-sm-4 text-center animated zoomInRight">
                                  <h1><i class="fa fa-check text-green-deep"></i></h1>
                              </div>
                              <div class="col-sm-8">
                                  <h4>Title of Course comes and sit right here</h4>
                                  <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                              </div>
                          </div>
                      </a>
                      
                      <a href="course" class="finis item link animated bounceIn">
                          <div class="row">
                             <div class="img-holder">
                                 <div class="img-holder"><img src="{{asset('assets/img/week1.jpg')}}" alt="" class="" width="120%"></div>
                             </div>
                              <div class="col-sm-4 text-center animated-slow zoomInRight">
                                  <h1><i class="fa fa-check"></i></h1>
                              </div>
                              <div class="col-sm-8">
                                  <h4>Title of Course comes and sit right here</h4>
                                  <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                              </div>
                          </div>
                      </a>
                      
                      <a href="course" class="finis item link animated bounceIn">
                          <div class="row">
<div class="img-holder">
                                 <div class="img-holder"><img src="{{asset('assets/img/week1.jpg')}}" alt="" class="" width="120%"></div>
                             </div>                              <div class="col-sm-4 text-center animated-slow zoomInRight">
                                  <h1><i class="fa fa-check"></i></h1>
                              </div>
                              <div class="col-sm-8">
                                  <h4>Title of Course comes and sit right here</h4>
                                  <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                              </div>
                          </div>
                      </a>
                      
                      <a href="course" class="finis item link">
                          <div class="row">
<div class="img-holder">
                                 <div class="img-holder"><img src="{{asset('assets/img/week1.jpg')}}" alt="" class="" width="120%"></div>
                             </div>                              <div class="col-sm-4 text-center">
                                  <h1><i class="fa fa-check"></i></h1>
                              </div>
                              <div class="col-sm-8">
                                  <h4>Title of Course comes and sit right here</h4>
                                  <p class="small text-muted"><i class="fa fa-clock-o"></i> Thu. 23rd May, 2016</p>
                              </div>
                          </div>
                      </a>
                      <a href="course" class="item link"><h1>9</h1></a>
                      <a href="course" class="finis item link"><h1>10</h1></a>
                    </div>
                </div>



                <div class="col-xs-12 hidden">
                <p class="text-center text-brandon text-uppercase space-lg">Week</p>
                  <div class="btn-group btn-group-justified no-margin" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                      <a href="week-1.php" type="button" class="btn-wk btn btn-success btn-lg text-brandon ">1</a>
                    </div>
                    <div class="btn-group" role="group">
                      <a href="week-2.php" type="button" class="btn-wk btn btn-default  text-brandon ">2</a>
                    </div>
                    <div class="btn-group" role="group">
                      <a href="week-3.php" type="button" class="btn-wk btn btn-default text-brandon ">3</a>
                    </div>
                    <div class="btn-group" role="group">
                      <a href="week-3.php" type="button" class="btn-wk btn btn-default  text-brandon ">4</a>
                    </div>
                    <div class="btn-group" role="group">
                      <a href="week-3.php" type="button" class="btn-wk btn btn-default text-brandon ">5</a>
                    </div>
                  </div>
                </div>

                
            </div>
        </div>
    </header>
<!--Header end-->


<section class="animated slideInUp">
   <div class="separator separator-sm"></div>
    <div class="container-fluid">
        <div class="row" style="">
            <div class="col-sm-6">
				<div class="panel panel-default panel-notify animated bounceIn">
						
							<div class="discussion-header bg-grey">
								<h4 class="no-margin text-center vla-orange-text">Latest Discussions &nbsp;<span class="label vla-red-bg text-white">3 new</span></h4>
							</div>
							<div id="" class="l-comment">
								<div class="media"><br>
									<div class="media-left">
										<a href="#">
											<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
										</a>
									</div>

									<div class="media-body">
										<h4 class="media-heading">Bolatito Ogunmodede <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria</a>
                                </h4>

										<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra...</p>

										<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>


									</div>
									<hr class="no-margin">
								</div>

								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Tope Oni <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria</a>
                                </h4>

										<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus...</p>

										<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>

									</div>
									<hr class="no-margin">
								</div>

								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Emmanuel Olamide Oni <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria, and the whole of Europe</a>
                                </h4>

										<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus...</p>

										<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>

									</div>
									<hr class="no-margin">
								</div>
								<div class="media"><br>
									<div class="media-left">
										<a href="#">
											<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
										</a>
									</div>

									<div class="media-body">
										<h4 class="media-heading">Bolatito Ogunmodede <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria</a>
                                </h4>

										<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra...</p>

										<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>


									</div>
									<hr class="no-margin">
								</div>

								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Tope Oni <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria</a>
                                </h4>

										<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus...</p>

										<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>

									</div>
									<hr class="no-margin">
								</div>

								<div class="media">
									<div class="media-left">
										<a href="#">
											<img class="media-object img-circle" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI3ZGM3MDIzNSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjdkYzcwMjM1Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNC41IiB5PSIzNi41Ij42NHg2NDwvdGV4dD48L2c+PC9nPjwvc3ZnPg==" data-holder-rendered="true" style="width: 64px; height: 64px;">
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">Emmanuel Olamide Oni <i class="text-muted small">in</i> <a href="">How to get a job in Nigeria, and the whole of Europe</a>
                                </h4>

										<p class="small comment">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus...</p>

										<p class="small"><a href="">Read Comment</a> <span class="pull-right text-muted">Thu. 29 May, 2016</span></p>

									</div>
									<hr class="no-margin">
								</div>
							</div>
						
					</div>
            </div>
          <div class="col-sm-6 ">
            <div class="well text-white text-center vla-yellow-bg" style="min-height: 439px; background:#45474d">
             <h1 class="lead vla-yellow-text">Olufemi, you are a 3 star Staff in the Academy</h1>
             <p class="lead">
                 <i class="fa fa-star-o"></i>
                 <i class="fa fa-star-o"></i>
                 <i class="fa fa-star-o"></i>
             </p>
             <p>Unlock more badges- take more courses. <br>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>

             <a href="">
                 <img style="border: 5px solid #fff;border-radius: 50%;margin: 2%;" width="25%" src="https://www.codeschool.com/assets/paths/badge-javascript-min-61c3e5858a6e6aee7aa5715f8d8824deaf087b6fa689df71451ea864805832f7.svg" alt="">
             </a>
             <a href="">
                 <img style="border: 5px solid #fff;border-radius: 50%;margin: 2%;" width="25%" src="https://www.codeschool.com/assets/paths/badge-ruby-min-d25a54bbab64a6e8b4fe1558748a49ee8c71887d0b65630a0cf0dab4791c57dc.svg" alt="">
             </a>
             <a href="">
                 <img style="border: 5px solid #fff;border-radius: 50%;margin: 2%;" width="25%" src="https://www.codeschool.com/assets/paths/badge-electives-min-04558aa07be6a8e932309a7575c158c1922d5ed80bb27d510a43087951d0855a.svg" alt="">
             </a>
             <br><br>
             <p>
                 <a href="#leadboard" class="btn btn-lg btn-default text-uppercase"><i class="fa fa-trophy"></i> &nbsp;see leaderboard</a>
             </p>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
    </div>
    <div class="separator separator-sm"></div>
</section>
    
<section class="text-center bg-grey">
    <div class="container">
       <div class="separator separator-sm"></div>
       
        <div class="row">
            <h4>More courses that might interest you.</h4><hr class="hr-xs" style="border-color:#ccc">
        </div>
        
        <div class="row">
<col-sm-10 class="col-sm-offset-1 col-md-10 col-md-offset-1">
                    <div class="col-md-4 col-sm-6  animated zoomIn">
                        <div class="panel panel-default course-item">
                            <a href="" class="text-center course-link">
                                <img src="{{asset('assets/img/week1.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
                                <h4>Successful Negotiation</h4>
                                <hr class="text-center course-underline">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
                                <br>
                                <span class="text-center col-xs-12">
                                <button class="btn btn-success ">View Course</button>	
                                </span>
                                <br><br>                         
                            </a>
                        </div>
                    </div>
    
                    <div class="col-md-4 col-sm-6 animated zoomIn">
                        <div class="panel panel-default course-item">
                            <a href="" class="text-center course-link">
                                <img src="{{asset('assets/img/week2.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
                                <h4>Positive Psychology</h4>
                                <hr class="text-center course-underline">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
                                <br>
                                <span class="text-center col-xs-12">
                                <button class="btn btn-success ">View Course</button>	
                                </span>
                                <br><br>
                            </a>
                        </div>
                    </div>
    
                    <div class="col-md-4 col-sm-6 animated zoomIn">
                                    <div class="panel panel-default course-item">
                                        <a href="" class="text-center course-link">
                                            <img src="{{asset('assets/img/week3.jpg')}}" alt="" class="img-rounded course-icon col-hidden-xs" width="100%">
                                            <h4>Business Foundations</h4>
                                            <hr class="text-center course-underline">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim atque ab, totam neque quam itaque. </p>
                                            <br>
                                            <span class="text-center col-xs-12">
                                            <button class="btn btn-success ">View Course</button>	
                                            </span>
                                            <br><br>
                                        </a>
                                    </div>
                                </div>
    </col-sm-10>
          </div>
        <a href="library/" class="text-center btn btn-default">See all available courses</a>
        <div class="separator separator-sm"></div>
    </div>
</section>

@stop


@section('script')

<script>
    $(document).ready(function() {

      $("#owl-demo").owlCarousel({
        items : 4,
        navigation : false,
        navigationText : ["<",">"]
      });

    //  $('.link').on('click', function(event){
    //    var $this = $(this);
    //    if($this.hasClass('clicked')){
    //      $this.removeAttr('style').removeClass('clicked');
    //    } else{
    //      $this.css('background','#7fc242').addClass('clicked');
    //    }
    //  });

    });
</script>
    
@endsection
<!-- /.container --> 