@extends('layout.main')

@section('content')


<!-- Page Content -->

<header class="course-header">
        <div class="container">
            <div class="row">
               <div class="col-sm-4">
                   <img src="{{asset('assets/img/week2.jpg')}}" alt="" class="img-top-core" width="100%">
               </div>
                <div class="col-sm-8">
                    <h1 class=" text-shadow-xs tagline text-bold">Title of Course comes here</h1>
                    <p class=" text-shadow-xs ">Description of Course comes here. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, laboriosam, laudantium maiores harum ab minus. Magnam ducimus quis fugiat debitis unde non, laborum quae, eos voluptas laudantium id dolor doloribus!</p>
                    <!-- <div class="separator-sm"></div> -->
                    <!-- <div class="progress">
                    <div class="one danger-color"></div><div class="two no-color"></div><div class="three no-color"></div>
                        <div class="progress-bar progress-bar-danger" style="width: 30%"></div>
                    </div> -->
                    <br>
                    <a href="course2" class="btn btn-lg btn-danger vla-red-bg text-brandon text-uppercase">  &nbsp; 25% Done &nbsp; | &nbsp; Continue <span class="hidden-xs">Course</span>  &nbsp; <i class="fa fa-chevron-right"></i></a>
                    
                </div>
                <div class="clearfix hidden-xs"><br></div><br>
            </div>
        </div>
    </header>  
               

<div class="container">

        <br>

        <div class="row">
            <div class="col-sm-12 col-md-8">
                <h2 class="">About the Course</h2>
                <p>Introduce the visitor to the business using clear, informative text. Use well-targeted keywords within your sentences to make sure search engines can find the business.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et molestiae similique eligendi reiciendis sunt distinctio odit? Quia, neque, ipsa, adipisci quisquam ullam deserunt accusantium illo iste exercitationem nemo voluptates asperiores.</p>
                <p>
                    <span class="text-muted small">Click / tap below to see what each week entails.</span>
                </p>
                <br>


                <h5 class="text-brandon text-uppercase space-lg">The Curriculum</h5>

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default panel-course">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">

                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          <div class="row">
                              <div class="col-sm-4">
                                <img src="{{asset('assets/img/week1.jpg')}}" alt="" class="img-cos" width="100%"><br>
                                <span class="fa-stack fa-2x fa-abs-x" title="You have marked this week as completed">
                                  <i class="fa fa-circle fa-stack-2x text-green-deep"></i>
                                  <i class="fa fa-check fa-stack-1x fa-inverse"></i>
                                </span>
                            </div>
                            <div class="col-sm-8">
                                <h3 class="">Week 1 : Module Nomenclature</h3>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas maiores velit tempore laudantium ipsam illo impedit ex minus eos! Repellendus, iste eos! Atque dolorum deserunt pariatur perferendis, ex assumenda!</p>
                                  <small class="">
                                    <span class="pull-right dotted-label"><i class="fa fa-clock-o"></i> 45min.</span>
                                    <span class="btn btn-sm btn-default">14 Classes &nbsp; | &nbsp; View Outline</span>
                                  </small>
                            </div>
                          </div>
                        </a>

                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                      <span href="" class="btn pull-left">Module Breakdown</span>
                        <a class="btn btn-default pull-right" href="class-video.php">Review Class »</a> 
                        <div class="clearfix"></div>
                        <div class="panel-body">

                          <table class="table"> 
                          <tbody> 
                              <tr> 
                                  <th scope="row">1.1</th> 
                                  <td><i class="fa fa-video-camera"></i> &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>35min</td> 
                              </tr> 
                              <tr> 
                                  <th scope="row">1.2</th> 
                                  <td><i class="fa fa-file-word-o"></i> &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>10min</td> 
                              </tr> 
                              <tr> <th scope="row">1.3</th> 
                                  <td><i class="fa fa-file-word-o"></i>  &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>15min</td> 
                              </tr> 
                              <tr> <th scope="row">1.4</th> 
                                  <td> <i class="fa fa-question-circle"></i>  &nbsp; Continous Assessment</td> 
                                  <td>15min</td> 
                              </tr> 
                          </tbody> 
                          </table>
                    </div>
                    </div>
                  </div>
                  <div class="panel panel-default panel-course active">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <div class="row">
                              <div class="col-sm-4">
                                <img src="{{asset('assets/img/week2.jpg')}}" alt="" class="img-cos" width="100%"><br>
                            </div>
                            <div class="col-sm-8">
                                <h3 class="">Week 2 : Module Nomenclature</h3>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas maiores velit tempore laudantium ipsam illo impedit ex minus eos! Repellendus, iste eos! Atque dolorum deserunt pariatur perferendis, ex assumenda!</p>
                                  <small class="">
                                    <span class="pull-right dotted-label"><i class="fa fa-clock-o"></i> 45min.</span>
                                    <span class="btn btn-sm btn-default">14 Classes &nbsp; | &nbsp; View Outline</span>
                                  </small>
                            </div>
                          </div>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <span href="" class="btn pull-left">Module Breakdown</span>
                    <a class="btn btn-success pull-right" href="class-video.php">Proceed to Class »</a> 
                    <div class="clearfix"></div>
                      <div class="panel-body">
                          <table class="table">  
                          <tbody> 
                              <tr> 
                                  <th scope="row">1.1</th> 
                                  <td><i class="fa fa-video-camera"></i> &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>35min</td> 
                              </tr> 
                              <tr> 
                                  <th scope="row">1.2</th> 
                                  <td><i class="fa fa-file-word-o"></i> &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>10min</td> 
                              </tr> 
                              <tr> <th scope="row">1.3</th> 
                                  <td><i class="fa fa-file-word-o"></i>  &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>15min</td> 
                              </tr> 
                              <tr> <th scope="row">1.4</th> 
                                  <td> <i class="fa fa-question-circle"></i>  &nbsp; Continous Assessment</td> 
                                  <td>15min</td> 
                              </tr> 
                          </tbody> 
                          </table>
                    </div>                   
                    </div>
                  </div>
                  <div class="panel panel-default panel-course">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          <div class="row">
                              <div class="col-sm-4">
                                <img src="{{asset('assets/img/week3.jpg')}}" alt="" class="img-cos" width="100%"><br>
                                <span class="fa-stack fa-2x fa-abs-x" title="You have not completed this module">
                                  <i class="fa fa-circle fa-stack-2x text-danger"></i>
                                  <i class="fa fa-exclamation fa-stack-1x fa-inverse"></i>
                                </span>
                            </div>
                            <div class="col-sm-8">
                                <h3 class="">Week 3 : Module Nomenclature</h3>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas maiores velit tempore laudantium ipsam illo impedit ex minus eos! Repellendus, iste eos! Atque dolorum deserunt pariatur perferendis, ex assumenda!</p>
                                  <small class="">
                                    <span class="pull-right dotted-label"><i class="fa fa-clock-o"></i> 45min.</span>
                                    <span class="btn btn-sm btn-default">14 Classes &nbsp; | &nbsp; View Outline</span>
                                  </small>                               
                            </div>
                          </div>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <div class="panel-body">
                          <table class="table"> 
                          <thead> 
                              <tr> 
                                  <td>Module Breakdown</td> 
                              </tr> 
                          </thead> 
                          <tbody> 
                              <tr> 
                                  <th scope="row">1.1</th> 
                                  <td><i class="fa fa-video-camera"></i> &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>35min</td> 
                              </tr> 
                              <tr> 
                                  <th scope="row">1.2</th> 
                                  <td><i class="fa fa-file-word-o"></i> &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>10min</td> 
                              </tr> 
                              <tr> <th scope="row">1.3</th> 
                                  <td><i class="fa fa-file-word-o"></i>  &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>15min</td> 
                              </tr> 
                              <tr> <th scope="row">1.4</th> 
                                  <td> <i class="fa fa-question-circle"></i>  &nbsp; Continous Assessment</td> 
                                  <td>15min</td> 
                              </tr> 
                          </tbody> 
                          </table>
                    </div>
                    </div>
                  </div>
                  <div class="panel panel-default panel-course">
                    <div class="panel-heading" role="tab" id="headingFour">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                          <div class="row">
                              <div class="col-sm-4">
                                <img src="{{asset('assets/img/week3.jpg')}}" alt="" class="img-cos" width="100%"><br>
                            </div>
                            <div class="col-sm-8">
                                <h3 class="">Week 3 : Module Nomenclature</h3>
                                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quas maiores velit tempore laudantium ipsam illo impedit ex minus eos! Repellendus, iste eos! Atque dolorum deserunt pariatur perferendis, ex assumenda!</p>
                                  <small class="">
                                    <span class="pull-right dotted-label"><i class="fa fa-clock-o"></i> 45min.</span>
                                    <span class="btn btn-sm btn-default">14 Classes &nbsp; | &nbsp; View Outline</span>
                                  </small>
                            </div>
                          </div>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                      <div class="panel-body">
                          <table class="table"> 
                          <thead> 
                              <tr> 
                                  <td>Module Breakdown</td> 
                              </tr> 
                          </thead> 
                          <tbody> 
                              <tr> 
                                  <th scope="row">1.1</th> 
                                  <td><i class="fa fa-video-camera"></i> &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>35min</td> 
                              </tr> 
                              <tr> 
                                  <th scope="row">1.2</th> 
                                  <td><i class="fa fa-file-word-o"></i> &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>10min</td> 
                              </tr> 
                              <tr> <th scope="row">1.3</th> 
                                  <td><i class="fa fa-file-word-o"></i>  &nbsp; Anim pariatur cliche reprehenderit</td> 
                                  <td>15min</td> 
                              </tr> 
                              <tr> <th scope="row">1.4</th> 
                                  <td> <i class="fa fa-question-circle"></i>  &nbsp; Continous Assessment</td> 
                                  <td>15min</td> 
                              </tr> 
                          </tbody> 
                          </table>
                    </div>
                    </div>
                  </div>
                  <!-- <div class="separator separator-sm"></div> --><br>
                </div>

            </div>

        <div class="col-md-4 col-sm-12">
         <h5 class="text-green-deep text-center text-brandon text-uppercase space-lg">meet your professors</h5><hr class="hr-xs">

            <div class="col-sm-6 col-md-12 text-center">
                <a class="prof" role="button" data-toggle="collapse" href="#collapseExample1" aria-expanded="false" aria-controls="collapseExample1"><img class="img-circle img-responsive img-center" width="65%" src="img/tewe.jpg" alt="">
                    <br>
                    <h4 class="no-margin">Lanre Olushola</h4>
                    <p> CEO, Insidify.net. Ghana</p></a>
                    
            <div class="col-xs-12 collapse" id="collapseExample1">
                <p class="small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis praesentium eaque fugiat doloremque dignissimos, eos vitae rerum labore quibusdam, autem accusantium ad ex, reiciendis ab nostrum quia ratione. Eos, ipsam!</p>
            </div><br>

            </div>
            <div class="col-sm-6 col-md-12 text-center">
                <a class="prof" role="button" data-toggle="collapse" href="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2"><img class="img-circle img-responsive img-center" width="65%" src="img/steve.jpg" alt="">
                    <br>
                    <h4 class="no-margin">Herbert Macaulay</h4>
                    <p> CEO, Insidify.net. Ghana</p></a>
                   
            <div class="col-xs-12 collapse" id="collapseExample2">
                <p class="small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis praesentium eaque fugiat doloremque dignissimos, eos vitae rerum labore quibusdam, autem accusantium ad ex, reiciendis ab nostrum quia ratione. Eos, ipsam!</p>
            </div> <br>
            </div>
            <div class="col-sm-6 col-md-12 text-center">
                <a class="prof" role="button" data-toggle="collapse" href="#collapseExample3" aria-expanded="false" aria-controls="collapseExample3"><img class="img-circle img-responsive img-center" width="65%" src="img/amanda.jpg" alt="">
                    <br>
                    <h4 class="no-margin">Chiamanada Oge</h4>
                    <p> CEO, Insidify.net. Ghana</p></a>
                   
            <div class="col-xs-12 collapse" id="collapseExample3">
                <p class="small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis praesentium eaque fugiat doloremque dignissimos, eos vitae rerum labore quibusdam, autem accusantium ad ex, reiciendis ab nostrum quia ratione. Eos, ipsam!</p>
            </div> <br>
            </div>
            <div class="col-sm-6 col-md-12 text-center">
                <a class="prof" role="button" data-toggle="collapse" href="#collapseExample4" aria-expanded="false" aria-controls="collapseExample4"><img class="img-circle img-responsive img-center" width="65%" src="img/olushola.jpg" alt="">
                    <br>
                    <h4 class="no-margin">Ernest Dudu</h4>
                    <p> CEO, Insidify.net. Ghana</p></a>
                   
            <div class="col-xs-12 collapse" id="collapseExample4">
                <p class="small">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis praesentium eaque fugiat doloremque dignissimos, eos vitae rerum labore quibusdam, autem accusantium ad ex, reiciendis ab nostrum quia ratione. Eos, ipsam!</p>
            </div> <br>
            </div>

        <div class="separator separator-sm clearfix"></div>
        </div>
          <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4"><br>
            <a href="class-video.php" class="btn btn-lg btn-warning text-brandon text-uppercase">  &nbsp; 25% Done &nbsp; | &nbsp; Continue <span class="hidden-xs">Course</span>  &nbsp; <i class="fa fa-chevron-right"></i></a>
          </div>
    </div>

    </div>
    
    <div class="separator separator-sm">
        <br>
    </div>

<!--
<section class="center-text">
    <div class="container">
        <div class="row">
            More courses section
        </div>
    </div>
</section>
-->




@stop


@section('script')

<script>
    $(document).ready(function() {

      $("#owl-demo").owlCarousel({
        items : 4,
        navigation : true,
        navigationText : ["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"],
        pagination: false
      });

    //  $('.link').on('click', function(event){
    //    var $this = $(this);
    //    if($this.hasClass('clicked')){
    //      $this.removeAttr('style').removeClass('clicked');
    //    } else{
    //      $this.css('background','#7fc242').addClass('clicked');
    //    }
    //  });

    });
</script>
    
@endsection
<!-- /.container --> 