@include('includes.head-meta')
@include('includes.header-staff')

<body class="">

@yield('content')   
    
<script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>


@include ('includes.footer')

@section('script')
    
@show
        
</body>

</html>
