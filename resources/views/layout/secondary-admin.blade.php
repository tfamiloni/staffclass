@include('includes.head-meta')
@include('includes.header-admin')

<body class="">

@yield('content')   
    
<script src="{{asset('assets/js/jquery.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>

@section('script')
    
@show
        
</body>

</html>
