@include('includes.head-meta')

<body class="bg-auth">

@yield('content')   
    
<!-- jQuery -->
<script src="{{asset('assets/js/jquery.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    
</body>

</html>
