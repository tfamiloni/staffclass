<!--
<section>
    <div class="container">
        <div class="row">
            
  
          <div class="col-sm-12 text-center"><br>
            <ul class="list-unstyled list-inline">
                
                <li><a href="http://www.viclawrence.com" target="_self">Home</a></li>
                
                <li><a href="contact" target="_self">Contact us</a></li>
                
                <li><a href="http://www.viclawrence.com/about-vla" target="_self">About VLA</a></li>
                
                <li><a href="what-we-do#outsourcing" target="_self">Outsourcing</a></li>
                
                <li><a href="what-we-do#recruitment" target="_self">Recruitment</a></li>
                
                <li><a href="what-we-do#training" target="_self">Training</a></li>
                
                <li><a href="blog" target="_self">VLA Knowledge</a></li>
                            
            </ul>
          </div>
          
          
        </div>
    </div>
</section>
-->

    <div class="col-sm-2 col-sm-offset-5 extension" style="margin-bottom: -3px;">
        <div class="row">
            <div class="col-xs-4 vla-red">&nbsp;</div>
            <div class="col-xs-4 vla-orange">&nbsp;</div>
            <div class="col-xs-4 vla-yellow">&nbsp;</div>
        </div>
    </div>


<footer>

<div class="container">
  <div class="row text-center">

  
      <div class="col-sm-12"><span class="hidden-sm hidden-md hidden-lg"><br></span>
          <ul class="list-unstyled footer-logo">
              <li>
                  <img src="{{asset('assets/img/logomark-x.png')}}" width="100px"><br>
                  <small><br> &copy; 2016. Vic Lawerence and Associates.</small>
              </li>
              
          </ul>
          <ul class="list-unstyled list-inline small ">
                
                <li><a class="vla-yellow-text" href="http://www.viclawrence.com" target="_blank">Home</a></li>
                
                <li><a class="vla-yellow-text" href="http://www.viclawrence.com/contact" target="_blank">Contact us</a></li>
                
                <li><a class="vla-yellow-text" href="http://www.viclawrence.com/about-vla" target="_blank">About VLA</a></li>
                
                <li><a class="vla-yellow-text" href="http://www.viclawrence.com/what-we-do#outsourcing" target="_blank">Outsourcing</a></li>
                
                <li><a class="vla-yellow-text" href="http://www.viclawrence.com/what-we-do#recruitment" target="_blank">Recruitment</a></li>
                
                <li><a class="vla-yellow-text" href="http://www.viclawrence.com/what-we-do#training" target="_blank">Training</a></li>
                
                <li><a class="vla-yellow-text" href="http://www.viclawrence.com/blog" target="_blank">VLA Knowledge</a></li>
                   
                <li><a class="vla-yellow-text" href="#!" target="_blank">VLA Academy</a></li>
                            
            </ul>
      </div>
<!--
          <div class="col-sm-2">
            <ul class="list-unstyled list-inline pull-right">
              <li><a href=""><i class="fa fa-sign-in"></i> &nbsp; Staff Login</a></li>
            </ul>
          </div>
-->
        </div>
</div>

</footer>


<script>

    $('#myTabs a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    });


    $('#btn-done').on( "click" ,function(){
        $(this)
        .addClass('btn-done')
        .find('.fa-stack i:eq(0)')
          .addClass('text-success animated rubberBand')
          .removeClass('text-muted');
    });

    $('#btn-done.btn-done').on('click',function(){
        $(this).unbind('click', function(){
          // $(this)
          //   .removeClass('btn-done')
          //   .find('.fa-stack i:eq(0)')
          //     .addClass('text-muted')
          //     .removeClass('text-success rubberBand');
            alert("sdsdsd");
        })
    });

</script>