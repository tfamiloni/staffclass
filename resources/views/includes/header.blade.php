    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="extension">
        <div class="row">
            <div class="col-xs-4 vla-red">&nbsp;</div>
            <div class="col-xs-4 vla-orange">&nbsp;</div>
            <div class="col-xs-4 vla-yellow">&nbsp;</div>
        </div>
    </div>    
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="img/logomark.png" alt="" height="35px"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right text-uppercase text-brandon">
                    <li>
                        <a href="_index.php">Home</a>
                    </li>
                    <li>
                        <a href="course-page.php">The curriculum</a>
                    </li>
                    <li>
                        <a href="page.php">About</a>
                    </li>
                    <li>
                        <a href="signin.php">Sign In</a>
                    </li>
                    <li>
                        <a href="signup.php" class="signup">Sign up</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>