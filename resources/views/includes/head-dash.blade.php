    <header class="course-header dash" style="">
        <div class="container">
            <div class="row">
                <div class="col-sm-12"><br>
                    <p class="lead text-center">Title of Course Comes here</p>
                    <div class="progression">

                        <div class="weekly active col-sm-2 col-sm-offset-1 col-xs-6">
                        <p class="small">Week One  
                                <span class="fa-stack pull-right">
                                  <i class="fa fa-circle fa-stack-2x fa-abs text-green-deep"></i>
                                  <i class="fa fa-check fa-stack-1x fa-inverse"></i>
                                </span> 
                        </p>
                        <a href="week-1.php">
                                <img src="img/week1.jpg" alt="week1" width="100%" height="">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                  </div>
                                </div>                      
                        </a>
                                          
                        </div>
                        <div class="weekly col-sm-2 col-xs-6">
                        <p class="small">Week Two <span class="pull-right">60%</span></p>
                        <a href="week-2.php">
                                <img src="img/week2.jpg" class="img-progress active" alt="week1" width="100%" height="">
                                <div class="progress">
                                  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                    <!-- <div class="step">60%</div> -->
                                  </div>
                                </div></a>
                            
                        </div>
                        <div class="weekly col-sm-2 col-xs-6">
                        <p class="small">Week Three</p>
                        <a href="week-3.php">
                                <img src="img/week3.jpg" alt="week1" width="100%" height="" class="translucent">
                                <div class="progress"></div></a>
                            
                        </div>
                        <div class="weekly col-sm-2 col-xs-6">
                        <p class="small">Week Four</p>
                        <a href="week-3.php">
                                <img src="img/week3.jpg" alt="week1" width="100%" height="" class="translucent">
                                <div class="progress"></div></a>
                            
                        </div>
                        <div class="weekly col-sm-2 col-xs-6">
                        <p class="small">Week Five</p>
                        <a href="week-3.php">
                                <img src="img/week3.jpg" alt="week1" width="100%" height="" class="translucent">
                                <div class="progress"></div></a>
                            
                        </div>
                    </div>
                </div>
                <div class="clearfix"><br></div>
                <div class="separator-sm"></div>
            </div>
        </div>
    </header>