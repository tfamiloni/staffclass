    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="extension">
        <div class="row">
            <div class="col-xs-4 vla-red">&nbsp;</div>
            <div class="col-xs-4 vla-orange">&nbsp;</div>
            <div class="col-xs-4 vla-yellow">&nbsp;</div>
        </div>
    </div>
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="{{asset('assets/img/vla-academy.png')}}" alt="" height="35px"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse text-brandon" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    
                    <li>
                        <a class=" text-uppercase text-" href="dashboard">dashboard</a>
                    </li>

                    <li>
                        <a class=" text-uppercase text-" href="manageStaff">staff</a>
                    </li>
                    
                    <li>
                        <a class=" text-uppercase text-" href="manageCourse">courses</a>
                    </li>

                    <li>
                        <a class=" text-uppercase text-" href="library">library</a>
                    </li>

                    <li id="fat-menu" class="dropdown"> 
		                <a id="drop3" href="#" class="dropdown-toggle" style="padding:5px" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
		                    <img src="http://dummyimage.com/300x300/E0271D/ffffff.jpg&text=MA" width="40px" class="img-profile" height="40px" alt="users name"> 
		                    <span class="caret"></span> 
	                    </a> 
	                    <ul class="dropdown-menu" aria-labelledby="drop3"> 
<!--		                    <li><a href="#">Dashboard</a></li> -->
		                    <li><a href="#">Contact Super Admin</a></li> 
		                    <li role="separator" class="divider"></li> 
		                    <li><a href="_index.php">Logout</a></li> 
	                    </ul> 
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    
   <script>
       
</script>