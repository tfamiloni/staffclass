<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function()
{
    return View::make('staff.signin');
});

Route::get('/forgot', function()
{
    return View::make('staff.forgot');
});
Route::get('/404', function()
{
    return View::make('staff.404');
});


Route::get('/test', function(){
	 return View::make('staff.dashboard');
});

Route::get('/staff/dashboard', function()
{
    return View::make('staff.dashboard');
});

Route::get('/staff/library', function()
{
    return View::make('staff.library');
});

Route::get('/staff/categories', function()
{
    return View::make('staff.categories');
});

Route::get('staff/course', function()
{
    return View::make('staff.course');
});

Route::get('staff/course2', function()
{
    return View::make('staff.course2');
});


Route::get('staff/course-discussion', function()
{
    return View::make('staff.course-discussion');
});


Route::get('staff/course-materials', function()
{
    return View::make('staff.course-materials');
});
Route::get('staff/class-article', function()
{
    return View::make('staff.class-article');
});
Route::get('staff/class-video', function()
{
    return View::make('staff.class-video');
});
Route::get('staff/class-test', function()
{
    return View::make('staff.class-test');
});
Route::get('staff/class-result', function()
{
    return View::make('staff.class-result');
});

Route::get('staff/my-courses', function()
{
    return View::make('staff.my-courses');
});

Route::get('staff/my-profile', function()
{
    return View::make('staff.my-profile');
});
Route::get('staff/profile', function()
{
    return View::make('staff.profile');
});
Route::get('staff/edit-profile', function()
{
    return View::make('staff.edit-profile');
});


Route::get('manager/dashboard', function()
{
    return View::make('manager.dashboard');
});
Route::get('admin/dashboard', function()
{
    return View::make('admin.dashboard');
});

Route::get('manager/addStaff', function()
{
    return View::make('manager.addStaff');
});

Route::get('admin/addStaff', function()
{
    return View::make('admin.addStaff');
});

Route::get('manager/staff', function()
{
    return View::make('manager.staff');
});
Route::get('admin/staff', function()
{
    return View::make('admin.staff');
});

Route::get('admin/addManager', function()
{
    return View::make('admin.addManager');
});

Route::get('manager/manageStaff', function()
{
    return View::make('manager.manageStaff');
});
Route::get('admin/manageStaff', function()
{
    return View::make('admin.manageStaff');
});
Route::get('admin/manageManager', function()
{
    return View::make('admin.manageManager');
});


Route::get('manager/addCourse', function()
{
    return View::make('manager.addCourse');
});
Route::get('admin/addCourse', function()
{
    return View::make('admin.addCourse');
});

Route::get('manager/module', function()
{
    return View::make('manager.module');
});
Route::get('admin/module', function()
{
    return View::make('admin.module');
});
Route::get('manager/module/addTest', function()
{
    return View::make('manager.addTest');
});
Route::get('manager/manageCourse', function()
{
    return View::make('manager.manageCourse');
});
Route::get('admin/manageCourse', function()
{
    return View::make('admin.manageCourse');
});

Route::get('manager/library', function()
{
    return View::make('manager.library');
});
Route::get('admin/library', function()
{
    return View::make('admin.library');
});

Route::get('manager/categories', function()
{
    return View::make('manager.categories');
});
Route::get('admin/categories', function()
{
    return View::make('admin.categories');
});